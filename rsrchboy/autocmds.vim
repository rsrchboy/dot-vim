"              _                           _
"   __ _ _   _| |_ ___   ___ _ __ ___   __| |___
"  / _` | | | | __/ _ \ / __| '_ ` _ \ / _` / __|
" | (_| | |_| | || (_) | (__| | | | | | (_| \__ \
"  \__,_|\__,_|\__\___/ \___|_| |_| |_|\__,_|___/
"
" ... one nice, easy place to (re)load them from

" apparently this needs to happen at sourcing time
let s:dir = expand('<sfile>:p:h')

augroup vimrc#filetype " {{{1
    au!

    " these have been moved to ftplugin/ files.
    "
    " ...mostly

    au FileType GV         setlocal nolist
    au FileType crontab    setlocal commentstring=#\ %s
    au FileType debcontrol setlocal commentstring=#\ %s
    au FileType extradite  nnoremap <buffer> <silent> <F1> :h extradite-mappings<CR>
    au FileType puppet     let b:vimpipe_command="T=`mktemp`; cat - > $T && puppet-lint $T; rm $T"
    au FileType startify   setlocal nonumber foldcolumn=0
    au FileType tmux       set tw=0
    au FileType tweetvim   setlocal nonumber foldcolumn=0
augroup end " }}}1

function! s:FugitiveBuffer() " {{{1
    " Well, this is annoying.  #User#Fugitive is dead, and now we need to
    " trigger this stuff manually if we want to make decisions on the basis of
    " a file being in a git repo or not.
    "
    " Note that we call FugitiveDetect() rather than just passing the bufnr to
    " FugitiveGitDir() as we want b:git_dir to be set properly -- and the
    " latter does not do that.
    "
    " https://github.com/tpope/vim-fugitive/commit/2386b9b39fb07adce95298c520c3290354a549cc

    call FugitiveDetect(+expand('<abuf>'))
    if empty(get(b:, 'git_dir', ''))
        return
    endif

    " restore #User#Fugitive, as much as we can.
    if exists('#User#Fugitive')
        if v:version >= 704 || (v:version == 703 && has('patch442'))
            doautocmd <nomodeline> User Fugitive
        elseif &modelines > 0
            let modelines = &modelines
            try
                set modelines=0
                doautocmd User Fugitive
            finally
                let &modelines = modelines
            endtry
        else
            doautocmd User Fugitive
        endif
    endif

    silent! Glcd

    nnoremap <buffer> <C-P> :GFiles --others --exclude-standard --cached<CR>

    if &ft == 'perl'
        let b:vimpipe_command  = 'perl -I ' . fugitive#repo().tree() . '/lib/ -'
    endif

    if !&readonly && !&diff
        " perform wip commits on save
        au vimrc#plugins BufWritePost <buffer> silent! call ducttape#git#wip()
    endif

    return
endfunction

augroup vimrc " {{{1
    au!

    au ColorScheme * execute ':runtime! after/colors/'.expand('<amatch>').".vim"

    " track buffers against tabs across sessions
    au TabLeave * silent! let g:buf2tab_previous_tabs_buffers = t:bufexp_buf_list
    au User Obsession call buf2tab#SaveTabInfo()
    au SessionLoadPost * call buf2tab#MyRestoreTabBuffers()

    au BufNewFile,BufRead .env  let b:ale_enabled = 0

    " initialize fugitive et al
    au BufNewFile,BufReadPost *      call <SID>FugitiveBuffer()
    autocmd FileType          netrw  call FugitiveDetect(+expand('<abuf>'), 0)

    " to be consistent with other bits of fugitive, etc
    au User FugitiveObject nnoremap <buffer> <silent> gq :q<CR>

    au Syntax * call VimrcCommonSyntax(expand('<amatch>'))
augroup end " }}}1

fun! s:PluginLoadedUserOperators() " {{{1
    if exists('*operator#user#define_ex_command')
        " we keep our operator definitions in here, so as to make life a
        " bit... easier.
        exe 'source' s:dir .. '/operators.vim'
    endif
    return
endfun

function! s:PluginLoadedExtradite() " {{{1

    " create the buffer-local :Extradite command
    silent! execute 'doautocmd User Fugitive'

    augroup vimrc#extradite#post_source_hook
        au!

        " ...and in buffers created before we loaded extradite, too
        au CmdUndefined Extradite :doautocmd User Fugitive
    augroup END
endfunction

function! s:PluginLoadedTabular() " {{{1

    " ...kinda.  assumes that the first '=' found is part of a fat comma
    AddTabularPattern first_fat_comma /^[^=]*\zs=>/l1
    AddTabularPattern first_equals    /^[^=]*\zs=/l1
    " FIXME!  commented out due to syntax (display) issues
    " AddTabularPattern first_squiggly  /^[^{]*\zs{/l1

    return
endfunction

function! s:GoBufRead() " {{{1

    if exists('g:go_loaded_install')
        au! vimrc#plugins BufRead,BufNewFile *.go
        return
    endif

    " au BufRead,BufNewFile *.go call plug#load('vim-go') | execute 'au! vimrc#plugins BufRead,BufNewFile *.go'
    call plug#load('vim-go')
    execute 'au! vimrc#plugins BufRead,BufNewFile *.go'
endfunction

augroup vimrc#plugins " {{{1
    " Here we try to include most of our plugin-related autocmds.  Some have
    " yet to be migrated, or are just friking messy, but having a 255 limit on
    " the number of augroups is low enough to make me think consolidation of
    " some sort isn't a horrible idea.
    au!

    au User tabular           call s:PluginLoadedTabular()
    au User vim-extradite     call s:PluginLoadedExtradite()
    au User vim-operator-user call s:PluginLoadedUserOperators()

    " au User vim-go nested edit
    au BufRead,BufNewFile *.go call s:GoBufRead()

    au VimEnter * call s:PluginLoadedUserOperators()

    " close git-auto-diff buffers automagically
    au BufWipeout .git/rebase-merge/git-rebase-todo :pc

    au VimEnter * call after_object#enable('=', ':', '-', '#', ' ')

    " Automatically remove fugitive buffers
    " autocmd BufReadPost fugitive://* set bufhidden=delete
    " au QuickFixCmdPost \[Location\ List\] :lopen<CR>
    " au User FugitiveGrepToLLPost :lopen<CR>

    " e.g. after we did something :Dispatchy, like :Gfetch
    au QuickFixCmdPost .git/**/index call fugitive#reload_status()

    " see also s:FugitiveBuffer()
    au BufEnter * silent! Glcd

    " refresh the gutters on writes to fugitive-controlled diff buffers (note
    " the "filename" indicates it's in diff mode
    " https://github.com/tpope/vim-fugitive/issues/1819
    au BufWritePost fugitive://*//[0-3]/* call gitgutter#all(1)
augroup END " }}}1

augroup vimrc#gnupg " {{{1
    au!

    " force the autocmds to run after we've loaded the plugin
    au User vim-gnupg nested edit
    au BufRead,BufNewFile *.{gpg,asc,pgp,pause} call plug#load('vim-gnupg') | execute 'au! vimrc#gnupg BufRead,BufNewFile'
augroup END " }}}1

function! s:BufEnterRefresh() " {{{1
    if !exists('g:_statusline_needs_refreshing')
        return
    endif
    unlet g:_statusline_needs_refreshing
    call rsrchboy#statuslineRefresh()
    return
endfunction

augroup vimrc#airline " {{{1
    au!

    " wipe on, say, :Dispatch or similar
    au QuickFixCmdPost      dispatch-make-complete call rsrchboy#statuslineRefresh()

    " a somewhat roundabout way to ensure the statusline of the window/buffer
    " we end up in is refreshed after a commit, so the subject is updated
    au User     FugitiveChanged  let g:_statusline_needs_refreshing = 1
    au BufEnter *                call s:BufEnterRefresh()

    " This is somewhat roundabout, but if we want to put the airline head
    " display back to something sensible this appears to be the only
    " reasonable way to do it w/o changing fugitive
    au OptionSet diff unlet! b:airline_head b:airline_head_subject |
    \   if has_key(w:, 'fugitive_diff_restore') |
    \       let w:fugitive_diff_restore .= '|unlet! b:airline_head b:airline_head_subject' |
    \   endif

    au FileChangedShellPost * call rsrchboy#statuslineRefresh()
    au ShellCmdPost         * call rsrchboy#statuslineRefresh()
    au FocusGained          * call rsrchboy#statuslineRefresh()
    au TabEnter             * unlet! b:airline_head b:airline_head_subject
    " au TabEnter,FocusGained * call rsrchboy#statuslineRefresh()
augroup END " }}}1


"au! User vim-airline call s:PluginLoadedAirline()

" FIXME: This was named incorrectly for some time; revalidate before
" reenabling

" " Do Things when the bundle is vivified
" function! s:PluginLoadedAirline()
"     let g:airline_section_a = airline#section#create_left(['mode', 'crypt', 'paste', 'capslock', 'tablemode', 'iminsert'])
" endfunction

"                                                _
"   ___ ___  _ __ ___  _ __ ___   __ _ _ __   __| |___
"  / __/ _ \| '_ ` _ \| '_ ` _ \ / _` | '_ \ / _` / __|
" | (_| (_) | | | | | | | | | | | (_| | | | | (_| \__ \
"  \___\___/|_| |_| |_|_| |_| |_|\__,_|_| |_|\__,_|___/
"
" ...such as they are

command! ReloadCommands :runtime rsrchboy/commands.vim
command! ReloadFuncs    :runtime rsrchboy/functions.vim
command! ReloadMaps     :runtime rsrchboy/mappings.vim

" transform blocks of text
command! -range -nargs=0 Snip       <line1>,<line2>call rsrchboy#snip#Snip()
command! -range -nargs=* Uniq       <line1>,<line2>! uniq
command! -range -nargs=* JsonTidy   <line1>,<line2>! json_xs -f json -t json-pretty
command! -range -nargs=* ColumnTidy <line1>,<line2>! /usr/bin/column -t
command! -range -nargs=* Cowsay     <line1>,<line2>! cowsay -W 65
command! -range -nargs=* BorgCowsay <line1>,<line2>! cowsay -W 65 -b
command! -range -nargs=* Figlet     <line1>,<line2>! figlet -w 78 -p
command! -range -nargs=* FigletU    <line1>,<line2>! figlet -w 78 -p -f utopia
command! -range -nargs=* FigletN    <line1>,<line2>! figlet -w 78 -p -f threepoint
command! -range -nargs=* PerlTidy   <line1>,<line2>! perltidy
command! -range -nargs=* MXRCize    <line1>,<line2>perldo perldo return unless /$NS/; s/$NS([A-Za-z0-9:]+)/\$self->\l$1_class/; s/::(.)/__\l$1/g; s/([A-Z])/_\l$1/g

" fzf " {{{1

command! -bang -nargs=* GGrep
    \ call fzf#vim#grep('git grep --line-number '.shellescape(<q-args>), 0, <bang>0)
command! -bang -nargs=0 Projects
    \ call rsrchboy#fzf#Projects(<bang>0)
command! -bang -nargs=0 Tabs
    \ call rsrchboy#fzf#Tabs(<bang>0)
command! -bang -nargs=0 Scripties
    \ call rsrchboy#fzf#Scriptnames(<bang>0)
command! -bang -nargs=0 Plugins
    \ call rsrchboy#fzf#Plugins(<bang>0)
" command! -bang -nargs=0 Autocmds
"     \ call rsrchboy#fzf#Autocmds(<bang>0)

" See help for *map-modes*
command! -bar -bang VMaps call fzf#vim#maps("v", <bang>0)
command! -bar -bang SMaps call fzf#vim#maps("s", <bang>0)
command! -bar -bang XMaps call fzf#vim#maps("x", <bang>0)
command! -bar -bang OMaps call fzf#vim#maps("o", <bang>0)
command! -bar -bang IMaps call fzf#vim#maps("i", <bang>0)
command! -bar -bang LMaps call fzf#vim#maps("l", <bang>0)
command! -bar -bang CMaps call fzf#vim#maps("c", <bang>0)
command! -bar -bang TMaps call fzf#vim#maps("t", <bang>0)

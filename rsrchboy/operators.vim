"                             _
"   ___  _ __   ___ _ __ __ _| |_ ___  _ __ ___
"  / _ \| '_ \ / _ \ '__/ _` | __/ _ \| '__/ __|
" | (_) | |_) |  __/ | | (_| | || (_) | |  \__ \
"  \___/| .__/ \___|_|  \__,_|\__\___/|_|  |___/
"       |_|
"
" A central location to define these.  This file will be sourced on startup
" when vim-user-operators is available.

" Notes on defining operators
"
" * operator#user#define() appears to hate counts; use
"   operator#user#define_ex_command() instead

" define the operators
call operator#user#define_ex_command('cutoffs', 'Snip')
map gZ <Plug>(operator-cutoffs)
call operator#user#define('tidy-json', 'rsrchboy#op#Op_ThingAndTidy', 'call rsrchboy#op#Set_op_command("JsonTidy")')
map tJ <Plug>(operator-tidy-json)
call operator#user#define_ex_command('tidy-column', 'ColumnTidy')
map tC <Plug>(operator-tidy-column)
call operator#user#define('tidy-figlet', 'rsrchboy#op#Op_ThingAndTidy', 'call rsrchboy#op#Set_op_command("Figlet")')
map tF <Plug>(operator-tidy-figlet)
call operator#user#define('tidy-figletu', 'rsrchboy#op#Op_ThingAndTidy', 'call rsrchboy#op#Set_op_command("FigletU")')
map tU <Plug>(operator-tidy-figletu)
call operator#user#define('tidy-figletn', 'rsrchboy#op#Op_ThingAndTidy', 'call rsrchboy#op#Set_op_command("FigletN")')
map tN <Plug>(operator-tidy-figletn)

"                            _
"  _ __ ___   __ _ _ __  ___| |
" | '_ ` _ \ / _` | '_ \/ __| |
" | | | | | | (_| | |_) \__ \_|
" |_| |_| |_|\__,_| .__/|___(_)
"                 |_|

" new!  (...kinda)

" https://github.com/rhysd/git-messenger.vim/issues/30#issuecomment-670222827
" nnoremap <silent> <Leader>gL :call setbufvar(winbufnr(popup_atcursor(split(system("git log -n 1 -L " . line(".") . ",+1:" . expand("%:p")), "\n"), { "padding": [1,1,1,1], "pos": "botleft", "wrap": 0 })), "&filetype", "git")<CR>

func! PopupGitMsg()
    if exists('b:diminactive')
        let b:diminactive_buf_state = b:diminactive
    else
        let b:diminactive_buf_state = -1
    endif
    :DimInactiveBufferOff
    call setbufvar(winbufnr(popup_atcursor(split(system("git log -n 1 -L " . line(".") . ",+1:" . expand("%:p")), "\n"), {
        \ "padding": [1,1,1,1],
        \ "pos": "botleft",
        \ "callback": function('PopupDimInactiveRestore', [ bufnr() ]),
        \ "wrap": 0 })), "&filetype", "git")
endfunc

fun! PopupDimInactiveRestore(buf, ...)
    " FIXME I'm not sure this approach is correct
    let l:bufstate = getbufvar(a:buf, 'diminactive_buf_state', -1)
    if l:bufstate == -1
        ":DimInactiveBufferReset
        exe ':' . a:buf . 'bufdo DimInactiveBufferReset'
    elseif l:bufstate == 1
        " :DimInactiveBufferOn
        exe ':' . a:buf . 'bufdo DimInactiveBufferOn'
    else
        " :DimInactiveBufferOff
        exe ':' . a:buf . 'bufdo DimInactiveBufferOff'
    endif
    " TODO FIXME should probably...
    " unlet b:diminactive_buf_state
    exe ':' . a:buf . 'bufdo unlet b:diminactive_buf_state'
endfun

" https://github.com/rhysd/git-messenger.vim/issues/30#issuecomment-670222827
nnoremap <silent> <Leader>gL :call PopupGitMsg()<CR>

" ...unrefactored, as of yet

nnoremap <silent> <Leader>td :split ~/todo.txt<CR>

nmap <silent> <leader>ttb :TagbarToggle<CR>

map gX <Plug>(operator-replace)

" we may (will) use this mapping largely outside of vim-ft files
nmap zS <Plug>ScripteaseSynnames

nnoremap <silent> <Leader>TT :TweetVimHomeTimeline<CR>
nnoremap <silent> <Leader>TS :TweetVimSay<CR>

nnoremap <Leader>Z :FloatermNew --disposable<CR>

nnoremap <silent> <Leader>gE :Extradite<CR>

" better-whitespace
nmap <silent> ,<space> :StripWhitespace<CR>

" incsearch
map /  <Plug>(incsearch-forward)

" tabular
nnoremap <silent> ,= :Tabularize first_fat_comma<CR>
nnoremap <silent> ,- :Tabularize first_equals<CR>
nnoremap <silent> ,{  :Tabularize first_squiggly<CR>
nnoremap <silent> ,}  :Tabularize /}/l1c0<CR>
nnoremap <silent> ,]  :Tabularize /]/l1c0<CR>
nnoremap <silent> ,)  :Tabularize /)/l1c0<CR>

" Easyalign:
xmap gA <Plug>(EasyAlign)
nmap gA <Plug>(EasyAlign)

" fzf
nnoremap <C-P>     :Files<CR>
nnoremap <C-L>     :Tabs<CR>
nnoremap <leader>O :Projects<CR>

" fugitive
nnoremap <silent> <Leader>gs :Git<Enter>
nnoremap <silent> <Leader>gd :Gdiff<CR>
nnoremap <silent> <Leader>gh :Gsplit HEAD^{}<CR>
nnoremap <silent> <Leader>gH :Gsplit -<CR>
" FIXME kinda broken as of late
" nnoremap <silent> <Leader>ga :call rsrchboy#git#add_to_index()<CR>
nnoremap <silent> <Leader>ga :Git add %<CR>
nnoremap <silent> <Leader>gc :Git commit<Enter>
nnoremap <silent> <Leader>gw :Git wwip<Enter>
nnoremap <silent> <Leader>gf :call rsrchboy#git#fixup()<CR>
nnoremap <silent> <Leader>gS :call rsrchboy#git#squash()<CR>
nnoremap <silent> <Leader>gp :Git push<CR>
nnoremap <silent> <Leader>gb :DimInactiveWindowOff<CR>:setl cursorline<CR>:Git blame -w -M -C<CR>

" GV
nnoremap <silent> <leader>gv :GV<CR>
nnoremap <silent> <leader>gV :GV!<CR>

" gitgutter textobj mappings
"
" The defaults -- ic/ac -- conflict with the excellent comments textobj plugin

omap iC <Plug>(GitGutterTextObjectInnerPending)
omap aC <Plug>(GitGutterTextObjectOuterPending)
xmap iC <Plug>(GitGutterTextObjectInnerVisual)
xmap aC <Plug>(GitGutterTextObjectOuterVisual)

" NOTE TODO: need mappings on the tmux side, as well as karabiner

" normal maps
nnoremap <silent> <S-Left>  <ESC>:TmuxNavigateLeft<cr>
nnoremap <silent> <S-Down>  <ESC>:TmuxNavigateDown<cr>
nnoremap <silent> <S-Up>    <ESC>:TmuxNavigateUp<cr>
nnoremap <silent> <S-Right> <ESC>:TmuxNavigateRight<cr>

" insert maps
inoremap <silent> <S-Left>  <ESC>:TmuxNavigateLeft<cr>
inoremap <silent> <S-Down>  <ESC>:TmuxNavigateDown<cr>
inoremap <silent> <S-Up>    <ESC>:TmuxNavigateUp<cr>
inoremap <silent> <S-Right> <ESC>:TmuxNavigateRight<cr>

" vimpipe
" load, then run.  this mapping will be overwritten on plugin load
execute 'nnoremap <silent> ' . g:vimpipe_invoke_map . " :call plug#load('vim-pipe') <bar> %call VimPipe()<CR>"

" from vimrc:

"" Alt: fix alt-key mappings " {{{2
""
"" This will have to be refined, but for now it fixes alt/meta mappings on:
""   lxterminal (tmux-256color)
""
"for i in range(97,122)
"  let c = nr2char(i)
"  exec "map  \e".c." <A-".toupper(c).">"
"  exec "map! \e".c." <A-".toupper(c).">"
"  exec "map  \e".c." <M-".toupper(c).">"
"  exec "map! \e".c." <M-".toupper(c).">"
"endfor " }}}2

nnoremap <Leader>SS :call rsrchboy#ShowSurroundMappings()<CR>
nnoremap <Leader>SM :call rsrchboy#ShowBufferMappings()<CR>
nnoremap <Leader>GM :call rsrchboy#cheats#mappings()<CR>
nnoremap <C-Z>      :shell<CR>

nnoremap <silent> <Leader>ft :filetype detect<CR>
nnoremap <silent> <F5>       :setlocal spell! spelllang=en_us<CR>

" make C-PgUp and C-PgDn work, even under screen
" see https://bugs.launchpad.net/ubuntu/+source/screen/+bug/82708/comments/1
nmap <ESC>[5;5~ <C-PageUp>
nmap <ESC>[6;5~ <C-PageDown>

cnoremap w!! w !sudo tee % >/dev/null

"   __                      _
"  / _|_   _ _ __   ___ ___| |
" | |_| | | | '_ \ / __/ __| |
" |  _| |_| | | | | (__\__ \_|
" |_|  \__,_|_| |_|\___|___(_)
"
" ...of doom!  (or not)

fun! CustomBranchName(name) " {{{1

    if (a:name ==# '') || get(b:, 'no_fancy_name', 0) || get(g:, 'no_fancy_name', 0)
        return a:name
    endif

    if (@% =~# '^fugitive://.*') && &diff

        " See ':h fugitive-revision'
        " fugitive:///home/rsrchboy/.vim/.git//0/...
        if @% =~# '^fugitive://.*//0/.*'
            return a:name
            " return '[INDEX] ' . a:name
        elseif @% =~# '^fugitive://.*//1/.*'
            return '[BASE] ' . a:name
        elseif @% =~# '^fugitive://.*//2/.*'
            return '[TARGET] ' . a:name
        elseif @% =~# '^fugitive://.*//3/.*'
            return '[MERGED] ' . a:name
        endif

    endif

    if &diff | return 'worktree(' . a:name . ')' | endif

    " skip fugitive buffers outright
    if @% =~# '^fugitive://.*'
        return ''
    endif

    " FIXME TODO for now...  just skip the below
    return l:info

    let l:info = a:name

    " This isn't perfect, but it does keep things from blowing up rather
    " loudly when we're editing a file that's actually a symlink to a file in
    " a git work tree.  (This appears to confuse vim-fugitive.)
    try
        let l:flags = ''
        try
            let l:ahead  = ducttape#git#revlist_count(a:name.'@{u}..HEAD')
            let l:behind = ducttape#git#revlist_count('HEAD..'.a:name.'@{u}')
            let l:flags .= ducttape#git#has_staged()   ? '!' : ''
            let l:flags .= ducttape#git#has_modified() ? '&' : ''
            " #status() for rebase, merge, etc
            " #has_staged(), #has_stash()
            let l:state = ducttape#git#state()
            let l:flags .= len(l:state) ? ' ' . toupper(l:state) : ''
        catch
            let l:ahead  = len(split(fugitive#repo().git_chomp('rev-list', a:name.'@{upstream}..HEAD'), '\n'))
            let l:behind = len(split(fugitive#repo().git_chomp('rev-list', 'HEAD..'.a:name.'@{upstream}'), '\n'))
        endtry
        let l:ahead  = l:ahead  ? 'ahead '  . l:ahead  : ''
        let l:behind = l:behind ? 'behind ' . l:behind : ''
        let l:commit_info = join(filter([l:ahead, l:behind], { idx, val -> val !=# '' }), ' ')
        let l:info .= l:flags
        let l:info .= len(l:commit_info) ? ' [' . l:commit_info . ']' : ''
    catch
        return a:name
    endtry

    return l:info
endfun

fun! VimrcCommonSyntax(syntax) " {{{1

    if &buftype != ""
        return
    endif

    if a:syntax =~# "^ref-"
        return
    endif

    if get(g:, 'vader_file', '') != ''
        return
    endif

    " ensure we only deal with the primary syntax, if multiples
    let l:syntax = substitute(a:syntax, "\\..*", "", "")

    " this is pretty dumb, but works in most cases to ensure that common
    " comment "HEY LOOK AT ME" highlights are applied

    execute 'syn match ' . l:syntax . 'Todo /\<\%(N\.B\.\ze\_s\|\%(NOTES\?\|TBD\|FIXME\|XXX\|PLAN\|TODO\)[:]\?\>\)/ containedin=' . l:syntax . 'Comment contains=NONE,@NoSpell'
endfun " }}}1

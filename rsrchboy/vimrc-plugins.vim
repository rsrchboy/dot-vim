"  ____  _             _           _
" |  _ \| |_   _  __ _(_)_ __  ___| |
" | |_) | | | | |/ _` | | '_ \/ __| |
" |  __/| | |_| | (_| | | | | \__ \_|
" |_|   |_|\__,_|\__, |_|_| |_|___(_)
"                |___/
"
" ...because dang, my vimrc seems to be... complex.

scriptencoding utf-8

" Configuration: " {{{1

" Most plugin settings don't need the overhead of their own section, so
" they're placed under 'general'.  If they're large, messy, or otherwise just
" don't play nicely with being in the general listing, then they're in their
" own sections below.

" general plugin configuration {{{2

" let g:solarized_termcolors = 256 " needed on terms w/o solarized palette
let g:GPGDefaultRecipients                            = ['0x84CC74D079416376', '0x1535F82E8083A84A']
let g:GPGFilePattern                                  = '\(*.\(gpg\|asc\|pgp\)\|.pause\)'
let g:GPGPreferArmor                                  = 1
let g:SuperTabNoCompleteAfter                         = ['^', '\s', '\\']
let g:UltiSnipsJumpBackwardTrigger                    = '<C-Left>'  " <c-k>
let g:UltiSnipsJumpForwardTrigger                     = '<C-Right>' " <c-j>
let g:airline#extensions#ale#enabled                  = 1
let g:airline#extensions#branch#format                = 'CustomBranchName'
let g:airline#extensions#bufferline#enabled           = 0
let g:airline#extensions#fugitiveline#enabled         = 0
let g:airline#extensions#hunks#non_zero_only          = 1
let g:airline#extensions#obsession#enabled            = 1
let g:airline#extensions#syntastic#enabled            = 0
let g:airline#extensions#tabline#buffer_nr_show       = 1
let g:airline#extensions#tabline#enabled              = 0
let g:airline#extensions#tabline#ignore_bufadd_pat    = '\c\vgundo|undotree|vimfiler|tagbar|nerd_tree|previewwindow|help|nofile'
let g:airline#extensions#tabline#show_close_button    = 0
let g:airline#extensions#tabline#tab_min_count        = 2
let g:airline#extensions#tagbar#enabled               = 1
let g:airline#extensions#tmuxline#enabled             = 0
let g:airline#extensions#whitespace#mixed_indent_algo = 1
let g:better_whitespace_filetypes_blacklist           = [ 'git', 'mail', 'help', 'startify', 'diff' ]
let g:bufExplorerShowRelativePath                     = 1
let g:bufExplorerShowTabBuffer                        = 1
let g:calendar_action                                 = 'vimwiki#diary#calendar_action'
let g:calendar_google_calendar                        = 1
let g:calendar_google_task                            = 1
let g:calendar_sign                                   = 'vimwiki#diary#calendar_sign'
let g:diminactive_buftype_blacklist                   = ['popup', 'nofile', 'nowrite', 'acwrite', 'quickfix', 'help']
let g:diminactive_enable_focus                        = 1
let g:diminactive_filetype_blacklist                  = ['startify', 'fugitiveblame']
let g:extradite_showhash                              = 1
let g:floaterm_autoclose                              = 1
let g:fugitive_gitlab_domains                         = ['https://git.eng.vindicia.com']
let g:fzf_history_dir                                 = '~/.local/share/fzf-history'
let g:gissues_lazy_load                               = 1 " github-issues plugin
let g:gist_clip_command                               = 'xclip -selection clipboard'
let g:gist_detect_filetype                            = 1
let g:gist_get_multiplefile                           = 1
let g:gist_post_private                               = 1
let g:gist_show_privates                              = 1
let g:github_dashboard                                = {}
let g:github_dashboard['emoji']                       = 1
let g:github_dashboard['rsrchboy']                    = 1
let g:github_upstream_issues                          = 1 " github-issues plugin
let g:go_auto_type_info                               = 1
let g:go_updatetime                                   = 250
let g:go_bin_path                                     = $VIMHOME . '/bin'
let g:go_fold_enable                                  = ['package_comment']
let g:go_highlight_build_constraints                  = 1 " syntax options -- disable on slower machines
let g:go_highlight_chan_whitespace_error              = 1 " syntax "issues", etc
let g:go_highlight_extra_types                        = 1 " syntax options -- disable on slower machines
let g:go_highlight_fields                             = 1 " syntax options -- disable on slower machines
let g:go_highlight_function_parameters                = 1 " syntax options -- disable on slower machines
let g:go_highlight_functions                          = 1 " syntax options -- disable on slower machines
let g:go_highlight_generate_tags                      = 1 " syntax options -- disable on slower machines
let g:go_highlight_methods                            = 1 " syntax options -- disable on slower machines
let g:go_highlight_operators                          = 1 " syntax options -- disable on slower machines
let g:go_highlight_types                              = 1 " syntax options -- disable on slower machines
let g:go_doc_popup_window                             = 1
let g:gutentags_exclude_filetypes                     = ['go']
let g:gutentags_exclude_project_root                  = ['/etc', '/usr/local', '~/.vim']
let g:gutentags_resolve_symlinks                      = 1
let g:neocomplete#enable_smart_case                   = 1
let g:neocomplete#sources#omni#input_patterns         = {}
let g:neocomplete#sources#omni#input_patterns.perl    = '\h\w*->\h\w*\|\h\w*::' " https://github.com/c9s/perlomni.vim
let g:neocomplete#sources#syntax#min_keyword_length   = 3
let g:notes_directories                               = [ '~/Shared/notes' ]
let g:notes_suffix                                    = '.notes'
let g:pandoc#filetypes#pandoc_markdown                = 0
let g:perl_no_extended_vars                           = 1
let g:perl_nofold_packages                            = 1 " we do this ourselves
let g:perl_sub_signatures                             = 1 " support highlighting for the new syntax
let g:polyglot_disabled                               = ['git', 'perl', 'systemd', 'tmux', 'csv', 'go', 'protobuf', 'markdown']
let g:scriptease_iskeyword                            = 0 " see note in our after/ftplugin/vim.vim
let g:snippets_dir                                    = '~/.vim/snippets,~/.vim/plugged/*/snippets'
let g:snips_author                                    = "Chris Weyl <cweyl@alumni.drew.edu>"
let g:solarized_termtrans                             = 1
let g:splitjoin_trailing_comma                        = 1
let g:sql_type_default                                = 'pgsql'
let g:startify_change_to_vcs_root                     = 1
let g:startify_custom_header                          = map(split(system('echo $USER@$HOST | figlet -t ; echo .; echo .; uname -a'), '\n'), '"   ". v:val') + ['','']
let g:startify_empty_buffer_key                       = 'o'
let g:startify_restore_position                       = 1
let g:startify_session_autoload                       = 1
let g:startify_session_detection                      = 1
let g:startify_session_persistence                    = 1
let g:startify_skiplist                               = [ 'COMMIT_EDITMSG', $VIMRUNTIME .'/doc', 'bundle/.*/doc' ]
let g:tagbar_autoclose                                = 1
let g:tagbar_autofocus                                = 1
let g:tagbar_left                                     = 1
let g:tmux_navigator_no_mappings                      = 1
let g:tweetvim_display_source                         = 1
let g:tweetvim_display_time                           = 1
let g:tweetvim_display_username                       = 1
let g:tweetvim_expand_t_co                            = 1
let g:tweetvim_open_buffer_cmd                        = '$tabnew'
let g:tweetvim_tweet_per_page                         = 50
let g:vim_markdown_auto_insert_bullets                = 0 " otherwise, problems with wrapped bullets... and stuff
let g:vim_markdown_fenced_languages                   = [ 'bash=sh', 'ini=dosini' ]
let g:vim_markdown_folding_disabled                   = 1
let g:vim_markdown_folding_style_pythonic             = 0
let g:vim_markdown_frontmatter                        = 1
let g:vim_markdown_new_list_item_indent               = 0 " otherwise, problems with wrapped bullets... and stuff
let g:vimpipe_close_map                               = ',p' " default mappings conflict with PerlHelp
let g:vimpipe_invoke_map                              = ',r' " default mappings conflict with PerlHelp
let g:vimwiki_list                                    = [{'path': '~/Shared/vimwiki/', 'path_html': '~/public_html/'}]
let g:vimwiki_use_calendar                            = 1
let g:zenburn_transparent                             = 1

" FIXME -- post-load autocmd!
" call deoplete#custom#option('omni_patterns', { 'go': '[^. *\t]\.\w*' })

" ale {{{2
" let g:ale_go_gopls_executable = $VIMHOME . '/bin/gopls'
" let g:ale_command_wrapper             = 'unshare --user --net --kill-child'
let g:ale_echo_msg_format             = '%linter% says %s'
let g:ale_set_loclist                 = 0 " when enabled, this clobbers :Glgrep
let g:ale_docker_allow                = 1
let g:ale_perl_perl_use_docker        = 'always'
let g:ale_perl_perlcritic_showrules   = 1
let g:ale_perl_perlcritic_as_warnings = 1
let g:ale_sh_shellcheck_exclusions    = 'SC2002,SC2086,SC2164'
let g:ale_type_map                    = { 'phpcs': { 'ES': 'WS', 'E': 'W' } }
let g:ale_linter_aliases = { 'vader': 'vim' }
let g:ale_linters                     = {
            \   'perl': [ 'perlcritic', 'proselint' ],
            \   'help': [ 'proselint'               ],
            \}
let g:ale_fix_on_save                 = 1
let g:ale_fixers                      = {
            \   'help': [ 'remove_trailing_lines', 'align_help_tags' ],
            \   'go': [ 'gofmt' ],
            \   'terraform': ['remove_trailing_lines', 'terraform', 'trim_whitespace'],
            \   'vim': ['remove_trailing_lines', 'trim_whitespace'],
            \}

let g:ale_yaml_ls_config = {
    \   'yaml': {
    \     'schemaStore': {
    \         'enable': v:true,
    \     },
    \   },
    \ }

" easyalign {{{2
" see https://github.com/junegunn/vim-easy-align#extending-alignment-rules

"   ? -> :? ternaries
"   ; ->  ;; (shell)
let g:easy_align_delimiters = {
\   '?': {
\       'pattern':         '[?:]',
\       'delimiter_align': 'c',
\       'align':           'll',
\   },
\   ';': { 'pattern': ';;' },
\ }

" airline {{{2

" N.B. extension variables are kept in with the general mix, above

let g:airline_theme = 'dark'

let g:airline_mode_map = {
    \ '__' : '-',
    \ 'n'  : 'N',
    \ 'i'  : 'I',
    \ 'R'  : 'R',
    \ 'c'  : 'C',
    \ 'v'  : 'V',
    \ 'V'  : 'V',
    \ '' : 'V',
    \ 's'  : 'S',
    \ 'S'  : 'S',
    \ '' : 'S',
    \ }

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

let g:airline_symbols.branch    = '⎇ '
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.linenr    = ''
let g:airline_left_sep          = ''
let g:airline_right_sep         = ''
let g:airline_detect_spell      = 0

" this appears to be behind certain slowdowns, so...
let g:no_fancy_name = 1

" lazylist {{{2

" the plugin author's configuration:

" nnoremap gli :LazyList
" vnoremap gli :LazyList

let g:lazylist_omap = 'il'
let g:lazylist_maps = [
                        \ 'gl',
                        \ {
                                \ 'l'  : '',
                                \ '*'  : '* ',
                                \ '-'   : '- ',
                                \ 't'   : '- [ ] ',
                                \ '2'  : '%2%. ',
                                \ '3'  : '%3%. ',
                                \ '4'  : '%4%. ',
                                \ '5'  : '%5%. ',
                                \ '6'  : '%6%. ',
                                \ '7'  : '%7%. ',
                                \ '8'  : '%8%. ',
                                \ '9'  : '%9%. ',
                                \ '.1' : '1.%1%. ',
                                \ '.2' : '2.%1%. ',
                                \ '.3' : '3.%1%. ',
                                \ '.4' : '4.%1%. ',
                                \ '.5' : '5.%1%. ',
                                \ '.6' : '6.%1%. ',
                                \ '.7' : '7.%1%. ',
                                \ '.8' : '8.%1%. ',
                                \ '.9' : '9.%1%. ',
                        \ }
                \ ]

" tagbar: perl {{{2

"    \ 'ctagsbin': 'perl-tags',
"    \ 'ctagsargs': '--outfile -',
let g:tagbar_type_perl = {
    \ 'sort' : 1,
    \ 'deffile' : '$HOME/.vim/ctags/perl',
    \ 'kinds' : [
        \ 'p:packages:1:0',
        \ 'u:uses:1:0',
        \ 'A:aliases:0:0',
        \ 'q:requires:1:0',
        \ 'c:constants:0:0',
        \ 'o:package globals:0:0',
        \ 'R:readonly:0:0',
        \ 'f:formats:0:0',
        \ 'e:extends',
        \ 'r:roles:1:0',
        \ 'a:attributes',
        \ 's:subroutines',
        \ 'l:labels',
        \ 'P:POD',
    \ ],
    \ 'sro' : '',
    \ 'kind2scope' : {
        \ 'p' : 'packages',
    \ },
    \ 'scope2kind' : {
        \ 'packages' : 'p',
        \ 'subroutines' : 's',
    \ },
\ }

" tagbar: puppet {{{2

let g:tagbar_type_puppet = {
    \ 'sort' : 1,
    \ 'ctagstype': 'puppet',
    \ 'deffile' : '$HOME/.vim/ctags/puppet',
    \ 'kinds' : [
        \ 'c:class:0:0',
        \ 's:site:0:0',
        \ 'n:node:0:0',
        \ 'd:definition:0:0',
    \ ],
\ }

" tagbar: go{{{2

" https://github.com/jstemmer/gotags#vim-tagbar-configuration
let g:tagbar_type_go = {
	\ 'ctagstype' : 'go',
	\ 'kinds'     : [
		\ 'p:package',
		\ 'i:imports:1',
		\ 'c:constants',
		\ 'v:variables',
		\ 't:types',
		\ 'n:interfaces',
		\ 'w:fields',
		\ 'e:embedded',
		\ 'm:methods',
		\ 'r:constructor',
		\ 'f:functions'
	\ ],
	\ 'sro' : '.',
	\ 'kind2scope' : {
		\ 't' : 'ctype',
		\ 'n' : 'ntype'
	\ },
	\ 'scope2kind' : {
		\ 'ctype' : 't',
		\ 'ntype' : 'n'
	\ },
	\ 'ctagsbin'  : 'gotags',
	\ 'ctagsargs' : '-sort -silent'
\ }

" committia {{{2

let g:committia_hooks = {}

function! g:committia_hooks.edit_open(info)

    " Scroll the diff window from insert mode
    imap <buffer> <PageDown> <Plug>(committia-scroll-diff-down-half)
    imap <buffer> <PageDown> <Plug>(committia-scroll-diff-up-half)

endfunction

" Plugins: " {{{1

" The 'Plug' command (understandably) barfs on script-local variables, so
" we're going to kinda kludge it with a global we'll use and unlet later.

let g:pluginLocalDirs = ['~/projects/vim', '~/work/vim']

let g:pluginOpts = {
\   'tabular': {
\       'on': [
\           'Tabularize',
\           'AddTabularPattern',
\           'AddTabularPipeline',
\       ],
\   },
\   'vim-closetag': {
\       'for': ['html', 'gohtmltmpl'],
\       'on': ['CloseTagEnableBuffer', 'CloseTagToggleBuffer'],
\   },
\ }

let s:has_python3_and_nvim = has('python3') &&  has('nvim') ? {} : { 'on': [] }
let s:has_python3_no_nvim  = has('python3') && !has('nvim') ? {} : { 'on': [] }

fun! s:MaybeDirs(dirs) " {{{2
    " let l:raw_dirs = globpath(join(g:pluginLocalDirs, ','), '*', 0, 1)
    let l:raw_dirs = globpath(join(a:dirs, ','), '*', 0, 1)
    let l:maybes = {}
    for l:dir in l:raw_dirs
        let l:name = fnamemodify(l:dir, ':t:s?\.git$??')
        let l:maybes[l:name] = l:dir
    endfor
    return l:maybes
endfun

fun! s:Plug(location) " {{{2
    " get/register; use global options if available
    let l:name = fnamemodify(a:location, ':t:s?\.git$??')
    " if we find a matching local dir, use it instead of the remote location
    let l:location = get(s:maybeLocalPluginDirs, l:name, a:location)
    call plug#(l:location, get(g:pluginOpts, l:name, {}))
endfun

fun! s:MaybeLocalPlugin(name, ...) abort " {{{2

    let l:prefix = a:0 ? a:1 : 'rsrchboy/'
    if l:prefix ==# 'gitlab'
        let l:prefix = 'https://gitlab.com/rsrchboy/'
    endif

    " this is getting a touch unwieldly
    if filereadable(expand('~/work/vim/' . a:name . '/.git/config'))
        Plug '~/work/vim/' . a:name
    elseif filereadable(expand('~/projects/vim/' . a:name . '/.git/config'))
        Plug '~/projects/vim/' . a:name
    elseif filereadable(expand('~/work/vim/' . a:name . '/.git')) " worktree
        Plug '~/work/vim/' . a:name
    else
        Plug l:prefix . a:name
    endif

endfun " }}}2

let s:maybeLocalPluginDirs = s:MaybeDirs(g:pluginLocalDirs)
let g:maybeLocalPluginDirs = s:maybeLocalPluginDirs

" to figure out which plugins have been loaded locally:
"
"   PP filter(copy(g:plugs), {k, v -> !has_key(v, 'uri') })

" Plugins: general bundles {{{2

Plug 'https://github.com/jlanzarotta/bufexplorer.git'
Plug 'https://github.com/AndrewRadev/splitjoin.vim.git'
Plug 'https://github.com/junegunn/vim-easy-align.git', { 'on': [ '<Plug>(EasyAlign)', 'EasyAlign' ] }
call s:Plug('https://github.com/godlygeek/tabular.git')
" Plug 'godlygeek/tabular', {
"             \   'on': [
"             \       'Tabularize',
"             \       'AddTabularPattern',
"             \       'AddTabularPipeline',
"             \   ],
"             \}
Plug 'https://github.com/SirVer/ultisnips.git'
Plug 'https://github.com/honza/vim-snippets.git'
Plug 'https://github.com/ervandew/supertab.git'
Plug 'https://github.com/haya14busa/incsearch.vim.git'
Plug 'https://github.com/ntpeters/vim-better-whitespace.git'
Plug 'https://github.com/mhinz/vim-startify.git'
Plug 'https://github.com/vim-airline/vim-airline.git'
call s:Plug('https://gitlab.com/rsrchboy/vim-airline-extensions.git')
Plug 'https://github.com/blueyed/vim-diminactive.git'
" XXX TRY THIS: mg979/vim-xtabline
" Plug 'https://github.com/mg979/vim-xtabline.git'
Plug 'https://github.com/jeffkreeftmeijer/vim-numbertoggle.git'
Plug 'https://github.com/rsrchboy/vim-togglecursor.git'
Plug 'https://github.com/tpope/vim-unimpaired.git'
Plug 'https://github.com/tpope/vim-sensible.git'
Plug 'https://github.com/tpope/vim-abolish.git'
Plug 'https://github.com/tpope/vim-vinegar.git'
Plug 'https://github.com/tpope/vim-endwise.git'
Plug 'https://github.com/tpope/vim-capslock.git'
Plug 'https://github.com/tpope/vim-characterize.git'
Plug 'https://github.com/tpope/vim-surround.git'
Plug 'https://github.com/tpope/vim-obsession.git'
Plug 'https://github.com/tpope/vim-commentary.git'
Plug 'https://github.com/tpope/vim-eunuch.git'
Plug 'https://github.com/tpope/vim-dispatch.git'
Plug 'https://github.com/Shougo/junkfile.vim.git'
Plug 'https://github.com/tpope/vim-speeddating.git'
Plug 'https://github.com/christoomey/vim-system-copy.git'
Plug 'https://github.com/junegunn/vader.vim.git'
Plug 'https://github.com/skywind3000/asyncrun.vim.git'
Plug 'https://github.com/Raimondi/delimitMate.git'
Plug 'https://github.com/moll/vim-bbye.git'
Plug 'https://github.com/easymotion/vim-easymotion.git'
Plug 'https://github.com/junegunn/vim-peekaboo.git'
" Plug 'https://github.com/terryma/vim-multiple-cursors.git'
Plug 'https://github.com/rhysd/vim-healthcheck.git'
call s:Plug('https://github.com/vim-scripts/loremipsum.git')

" Plugins: library plugins/bundles {{{2

Plug 'https://github.com/tomtom/tlib_vim.git'
Plug 'https://github.com/xolox/vim-misc.git'
Plug 'https://github.com/vim-scripts/ingo-library.git'
Plug 'https://github.com/vim-scripts/CountJump.git'
Plug 'https://github.com/mattn/webapi-vim.git'
Plug 'https://github.com/junegunn/vim-emoji.git'
Plug 'https://github.com/Shougo/context_filetype.vim.git'
Plug 'https://github.com/tpope/vim-repeat.git'
Plug 'https://github.com/ryanoasis/vim-devicons.git'

" Plugins: appish or external interface {{{2
"
Plug 'https://github.com/previm/previm.git'
Plug 'https://github.com/thinca/vim-ref.git'
Plug 'https://github.com/keith/travis.vim.git', { 'on': 'Travis' }
Plug 'https://github.com/junegunn/fzf.git', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'https://github.com/junegunn/fzf.vim.git'
Plug 'https://github.com/codegram/vim-codereview.git' ", { 'on': 'CodeReview' }
Plug 'https://github.com/w0rp/ale.git'
Plug 'https://github.com/jaxbot/github-issues.vim.git', { 'on': ['Gissues', 'Gmiles', 'Giadd'] }
Plug 'https://github.com/krisajenkins/vim-pipe.git', { 'on': [] }
Plug 'https://github.com/xolox/vim-notes.git', { 'on': 'Note' }
Plug 'https://github.com/vim-scripts/dbext.vim.git', {
            \   'on': [
            \       'DBDescribe',
            \       'DBExec',
            \       'DBList',
            \       'DBPrompt',
            \       'DBSelect',
            \   ],
            \}
Plug 'https://github.com/itchyny/calendar.vim.git', { 'on': 'Calendar' }
Plug 'https://github.com/pentie/VimRepress.git', { 'on': ['BlogNew', 'BlogOpen', 'BlogList'] }
Plug 'https://github.com/aquach/vim-mediawiki-editor.git', {
            \   'for': 'mediawiki',
            \   'on': ['MWRead', 'MWWrite', 'MWBrowse'],
            \}
Plug 'https://github.com/tmux-plugins/vim-tmux-focus-events.git'
Plug 'https://github.com/christoomey/vim-tmux-navigator.git'
Plug 'https://github.com/vim-scripts/vimwiki.git', {
            \ 'on': [
            \     'Vimwiki',
            \     'VimwikiIndex',
            \     '<Plug>Vimwiki',
            \ ],
            \}
Plug 'https://github.com/voldikss/vim-floaterm.git'
"Plug 'https://github.com/voldikss/fzf-floaterm.git'

" Plugins: neocomplete / deoplete / etc {{{2

Plug 'https://github.com/Shougo/neoinclude.vim.git'
" FIXME clean this up!
if has('python3')
  if has('nvim')
    Plug 'https://github.com/Shougo/deoplete.nvim.git', { 'do': ':UpdateRemotePlugins' }
    " Plug 'https://github.com/Shougo/deoplete.nvim.git', s:has_python3_and_nvim
  else
    Plug 'https://github.com/Shougo/deoplete.nvim.git', { 'do': 'pip3 install --user pynvim' }
    Plug 'https://github.com/roxma/nvim-yarp.git'
    Plug 'https://github.com/roxma/vim-hug-neovim-rpc.git'
  endif
  " TODO needs a check for a certain python module, IIRC
  " Plug 'https://github.com/zchee/deoplete-go.git', { 'for': 'go', 'do': 'make' }
endif
Plug 'https://github.com/Shougo/neco-vim.git',   { 'for': 'vim' }
Plug 'https://github.com/c9s/perlomni.vim.git',  { 'for': 'perl' }
Plug 'https://github.com/Shougo/neco-syntax.git'

" Plugins: git and version controlish {{{2

Plug 'https://github.com/airblade/vim-gitgutter.git'
Plug 'https://github.com/rsrchboy/vim-extradite.git', { 'on': 'Extradite' }
Plug 'https://github.com/tpope/vim-rhubarb.git'
Plug 'https://github.com/shumphrey/fugitive-gitlab.vim.git'
Plug 'https://github.com/rsrchboy/vim-fubitive.git' " upstream isn't paying attention
Plug 'https://github.com/tpope/vim-fugitive.git'
" Plug 'https://github.com/sodapopcan/vim-twiggy'
Plug 'https://github.com/mattn/gist-vim.git', { 'on': 'Gist' }
Plug 'https://github.com/tpope/vim-git.git'
Plug 'https://github.com/junegunn/gv.vim.git', { 'on': 'GV' }
Plug 'https://github.com/rhysd/conflict-marker.vim.git'
" NOTE appears to be causing wonky buffer/window shifts as of 28Nov2022
Plug 'https://github.com/rhysd/committia.vim.git'
Plug 'https://github.com/hotwatermorning/auto-git-diff.git'
Plug 'https://github.com/rhysd/git-messenger.vim.git'
" for svn
" Plug 'https://github.com/juneedahamed/vc.vim.git', { 'on': [
"             \ 'VCInfo', 'VCStatus', 'VDDiff', 'VCBlame', 'VCLog'] }

" Plugins: GitHub {{{2

Plug 'https://github.com/junegunn/vim-github-dashboard.git', { 'on': ['GHA', 'GHD', 'GHDashboard', 'GHActivity'] }
Plug 'https://github.com/jez/vim-github-hub.git'
Plug 'https://github.com/rhysd/github-complete.vim.git'
Plug 'https://github.com/rhysd/vim-gfm-syntax.git'

" Plugins: Twitter {{{2

Plug 'https://github.com/basyura/TweetVim.git',
            \ { 'on': [
            \   'TweetVimAddAccount',
            \   'TweetVimCommandSay',
            \   'TweetVimHomeTimeline',
            \   'TweetVimSay',
            \ ] }
Plug 'https://github.com/basyura/twibill.vim.git'
Plug 'https://github.com/basyura/bitly.vim.git'
Plug 'https://github.com/tyru/open-browser.vim.git'
Plug 'https://github.com/mattn/favstar-vim.git', { 'on': 'FavStar' }

" Plugins: Perl {{{2

Plug 'https://github.com/vim-perl/vim-perl.git', { 'branch': 'dev' }
Plug 'https://github.com/LStinson/perlhelp-vim.git', { 'on': ['PerlHelp', 'PerlMod'] }
Plug 'https://github.com/vim-scripts/log4perl.vim.git'
call s:MaybeLocalPlugin('vim-ducttape')
call s:MaybeLocalPlugin('vim-ducttape-git', 'gitlab')
call s:MaybeLocalPlugin('vim-ducttape-gitlab', 'gitlab')
call s:MaybeLocalPlugin('vim-ducttape-frontmatter', 'gitlab')
call s:MaybeLocalPlugin('vim-ducttape-fugitive', 'gitlab')
" Plug 'https://github.com/rsrchboy/update_perl_line_directives.git', { 'for': 'vim' }
" Plug 'https://github.com/rsrchboy/syntax_check_embedded_perl.vim.git', { 'on':  []    }

" Install these for use by ducttape
" let g:pluginOpts.p5_git_raw " {{{3
"
" This fork of Git::Raw has Git::CommitBuild enabled in its dzil config, so
" any builds get committed to the build branch.
"
let g:pluginOpts.p5_git_raw = {
    \   'branch': 'build/ditch-embedded-libgit2',
    \   'on': [],
    \   'do': ':call ducttape#install("--notest .")'
    \}
" }}}3
Plug 'https://github.com/rsrchboy/p5-Git-Raw.git', g:pluginOpts.p5_git_raw

" Plugins: syntax / filetype {{{2

Plug 'https://github.com/tpope/vim-apathy.git'
Plug 'https://github.com/wfxr/protobuf.vim.git'
Plug 'https://github.com/sheerun/vim-polyglot.git'
" Plug 'https://github.com/vim-pandoc/vim-pandoc.git', { 'for': 'pandoc' }
Plug 'https://github.com/plasticboy/vim-markdown.git'
if v:version >= 800
    Plug 'https://github.com/fatih/vim-go.git', { 'do': ':GoInstallBinaries' }
endif
Plug 'https://github.com/fmoralesc/vim-pinpoint.git'
Plug 'https://github.com/vim-scripts/gtk-vim-syntax.git'
Plug 'https://github.com/chikamichi/mediawiki.vim.git', { 'for': 'mediawiki' }
Plug 'https://github.com/yko/mojo.vim.git'
" Plug 'https://github.com/andrewstuart/vim-kubernetes.git'
Plug 'https://github.com/tpope/vim-afterimage.git'
Plug 'https://github.com/lervag/vimtex.git', { 'for': ['tex'] }
Plug 'https://github.com/jamessan/vim-gnupg.git', { 'on': [] }
Plug 'https://github.com/mzlogin/vim-markdown-toc.git', { 'for': 'markdown' } " {{{3
" Plug 'iamcco/markdown-preview.nvim', ... {{{3

if v:version >= 800 || has('nvim')
    " https://github.com/iamcco/markdown-preview.nvim#install--usage
    if executable('yarn') && executable('node')
        Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install'  }
    elseif executable('npm') && executable('node')
        Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & npm install'  }
    else
        Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
    endif
endif

" }}}3
" Plug 'https://github.com/SidOfc/mkdx.git', { 'for': ['markdown'] }
" N.B. for scripease, only set up autoload commands for those we may
" reasonably invoke while _not_ editing VimL, as loading any file with a vim
" f/t will cause the plugin to be loaded.
Plug 'https://github.com/tpope/vim-scriptease.git',
            \ {
            \   'on': [
            \       '<Plug>ScripteaseSynname',
            \       'Messages',
            \       'PP',
            \       'PPmsg',
            \       'Scriptnames',
            \       'Time',
            \       'Verbose',
            \   ],
            \   'for': 'vim',
            \ }
" Plug 'https://github.com/xolox/vim-lua-ftplugin.git',                  { 'for': 'lua' }
" Plug 'https://github.com/xolox/vim-lua-inspect.git',                   { 'for': 'lua' }
" Plug 'https://github.com/WolfgangMehner/lua-support.git',              { 'for': 'lua' }
Plug 'https://github.com/mattn/emmet-vim.git', { 'for': 'html' }
Plug 'https://github.com/nono/jquery.vim.git'
" Plug 'https://github.com/klen/python-mode.git',                        { 'for': 'python' }
" FIXME the next might be a good candidate for a polyglot PR...
Plug 'https://github.com/chrisbra/csv.vim.git',                        { 'for': 'csv', 'on': 'CSVTable' }
Plug 'https://github.com/vadv/vim-chef.git',                { 'for': 'chef'   }
Plug 'https://github.com/tmux-plugins/vim-tmux.git'
Plug 'https://github.com/apeschel/vim-syntax-syslog-ng.git'
Plug 'https://github.com/vim-pandoc/vim-pandoc-syntax.git', { 'for': 'pandoc' }
Plug 'https://github.com/ap/vim-css-color.git', { 'on': [] }
Plug 'https://github.com/jparise/vim-graphql.git'
Plug 'https://github.com/yoppi/fluentd.vim.git'
" Plug 'itkq/fluentd-vim'
Plug 'https://github.com/robertbasic/vim-hugo-helper.git', { 'for': 'markdown' }
call s:Plug('https://github.com/alvan/vim-closetag.git')
Plug 'https://github.com/rhysd/vim-syntax-codeowners.git'

" Plugins: systemy bits {{{2

Plug 'https://github.com/qnighy/vim-ssh-annex.git'
Plug 'https://github.com/tmatilai/gitolite.vim.git'
Plug 'https://github.com/eiginn/iptables.git'
Plug 'https://github.com/RsrchBoy/interfaces.git'
Plug 'https://github.com/chr4/nginx.vim.git'
Plug 'https://github.com/smancill/conky-syntax.vim.git'
Plug 'https://github.com/FredDeschenes/httplog.git'
Plug 'https://github.com/vim-scripts/openvpn.git', { 'for': 'openvpn' }
" Plug 'https://github.com/chr4/sslsecure.vim.git'

" Plugins: s/w packaging: deb, arch, etc {{{2

Plug 'https://github.com/vim-scripts/deb.vim.git'
Plug 'https://github.com/Firef0x/PKGBUILD.vim.git', { 'for': 'PKGBUILD' }

" Plugins: text objects {{{2

" See also https://github.com/kana/vim-textobj-user/wiki
Plug 'https://github.com/kana/vim-textobj-user.git'
Plug 'https://github.com/kana/vim-textobj-syntax.git'
Plug 'https://github.com/kana/vim-textobj-diff.git'
Plug 'https://github.com/kana/vim-textobj-indent.git'
Plug 'https://github.com/kana/vim-textobj-entire.git'
Plug 'https://github.com/glts/vim-textobj-comment.git'
call s:MaybeLocalPlugin('vim-textobj-heredocs')
Plug 'https://github.com/reedes/vim-textobj-quote.git'
Plug 'https://github.com/kana/vim-textobj-line.git'
Plug 'https://github.com/rhysd/vim-textobj-conflict.git'
" Plug 'https://github.com/Julian/vim-textobj-brace.git'
Plug 'https://github.com/Julian/vim-textobj-variable-segment.git'
Plug 'https://github.com/kana/vim-textobj-function.git'
Plug 'https://github.com/sgur/vim-textobj-parameter.git'
" note: php/python/ruby/etc helpers exist, if we start dabbling there
Plug 'https://github.com/thinca/vim-textobj-function-perl.git', { 'for': 'perl' }
Plug 'https://github.com/vimtaku/vim-textobj-sigil.git',        { 'for': 'perl' }
Plug 'https://github.com/spacewander/vim-textobj-lua.git',      { 'for': 'lua'  }
Plug 'https://github.com/akiyan/vim-textobj-php.git',           { 'for': 'php'  }
" Plug 'https://github.com/kana/vim-textobj-help.git',          { 'for': 'help' }
" Plug 'https://github.com/nelstrom/vim-textobj-rubyblock.git', { 'for': 'ruby' }
Plug 'https://github.com/junegunn/vim-after-object.git'

" Plugins: operators {{{2

Plug 'https://github.com/christoomey/vim-sort-motion.git'
Plug 'https://github.com/kana/vim-operator-user.git'
Plug 'https://github.com/kana/vim-operator-replace.git'

" Plugins: color schemes: {{{2

Plug 'https://github.com/sainnhe/edge.git'
Plug 'https://github.com/flazz/vim-colorschemes.git'
Plug 'https://github.com/Reewr/vim-monokai-phoenix.git'
Plug 'https://github.com/tomasr/molokai.git'
Plug 'https://github.com/jnurmine/Zenburn.git'
Plug 'https://github.com/altercation/vim-colors-solarized.git'

" Plugins: other {{{2

Plug 'https://github.com/ludovicchabant/vim-gutentags.git'
Plug 'https://github.com/majutsushi/tagbar.git', { 'on': [ 'Tagbar', 'TagbarToggle' ] }
Plug 'https://github.com/freitass/todo.txt-vim.git', { 'for': 'todo' }
Plug 'https://github.com/KabbAmine/lazyList.vim.git', { 'on': 'LazyList' }
Plug 'https://github.com/mattn/googletasks-vim.git', { 'on': 'GoogleTasks' }

" Plugins: Jira Integration {{{2

" I don't use jira right now, but...

Plug 'https://github.com/mnpk/vim-jira-complete.git', {'on': []}
Plug 'https://github.com/RsrchBoy/vim-jira-open.git', {'on': []}

" }}}1

"  _        _       _         _             _
" | |_ _ __(_) __ _| |  _ __ | |_   _  __ _(_)_ __  ___
" | __| '__| |/ _` | | | '_ \| | | | |/ _` | | '_ \/ __|
" | |_| |  | | (_| | | | |_) | | |_| | (_| | | | | \__ \
"  \__|_|  |_|\__,_|_| | .__/|_|\__,_|\__, |_|_| |_|___/
"                      |_|            |___/
"
" Included with all the trimmings.  if we keep them, then we'll properly sort
" the config/au/maps/etc out.

" special case this -- last thing we want is copilot ingesting a bunch of
" secrets!  This isn't ideal... But it seems to be the best way to handle it.
"
" sops appears to decrypt files for editing under /tmp/\d+, so...

if @% !~# '^/tmp/[0123456789]*/'
    Plug 'https://github.com/github/copilot.vim.git'
endif

Plug 'https://github.com/ferrine/md-img-paste.vim.git'

autocmd vimrc#plugins FileType markdown nnoremap <buffer> <silent> <leader>v :call mdip#MarkdownClipboardImage()<CR>

" Plug 'https://github.com/junegunn/vim-slash.git'
Plug 'https://github.com/junegunn/vim-journal.git'

" Handle symlinks in Vim
Plug 'https://github.com/aymericbeaumet/vim-symlink.git'
" Plug 'https://github.com/moll/vim-bbye.git' " recommended dependency of the above; already installed

" new, improved! database support
" Plug 'https://github.com/tpope/vim-dadbod.git'
" Plug 'https://github.com/kristijanhusak/vim-dadbod-ui.git


" | _  _  _     _  _  _ __ _ _  _  _  _   _    _  _  _  __|_
" |(_|| |(_||_|(_|(_|(/_  _\(/_|\/(/_|   _\|_||_)|_)(_)|  |
"         _|       _|                         |  |

" FIXME ???
Plug 'https://github.com/b0o/SchemaStore.nvim.git'

" Plug 'dense-analysis/ale'  " done above
Plug 'https://github.com/prabirshrestha/vim-lsp.git'
Plug 'https://github.com/rhysd/vim-lsp-ale.git'

" https://github.com/neoclide/coc.nvim/wiki/Using-coc-extensions#implemented-coc-extensions

" " for now, though I suspect it means little, really
"             " \ 'coc-gist',
" let g:coc_global_extensions = get(g:, 'coc_global_extensions', []) + [
"             \ 'coc-marketplace',
"             \ 'coc-css',
"             \ 'coc-docker',
"             \ 'coc-go',
"             \ 'coc-json',
"             \ 'coc-perl',
"             \ 'coc-toml',
"             \ 'coc-vimlsp',
"             \ 'coc-yaml',
"             \ ]

" " N.B. integrate w/healthcheck?
" if executable('npm') && executable('node')
"     Plug 'https://github.com/neoclide/coc.nvim.git', {'branch': 'release'}
" endif

set shortmess+=c

" vim: set foldmethod=marker foldcolumn=5 fdls=0 :

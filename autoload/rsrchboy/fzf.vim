" Functions supporting our use of fzf

for s:eval in ducttape#symbiont#autoload(expand('<sfile>'))
    execute s:eval
endfor

let g:rsrchboy#fzf#project_dirs = get(g:, 'rsrchboy#fzf#project_dirs',
    \ '~/work ~/.vim ~/.tmux ~/.stow-dotfiles ~/projects')
let g:rsrchboy#fzf#additional_project_dirs
    \ = get(g:, 'rsrchboy#fzf#additional_project_dirs')
let g:rsrchboy#fzf#search_depth = get(g:, 'rsrchboy#fzf#search_depth', 3)

" BEGIN ======> from fzf.vim
"
" N.B. This is starting out here, but likely will be abstracted out into its
" own autoload section.
"
" Based off of:
"
" https://github.com/junegunn/fzf.vim/blob/fd7fab77b20fdf09180cabdd660d8c3b7fe7ca1a/autoload/fzf/vim.vim#L230-L265

function! s:get_color(attr, ...)
  let gui = has('termguicolors') && &termguicolors
  let fam = gui ? 'gui' : 'cterm'
  let pat = gui ? '^#[a-f0-9]\+' : '^[0-9]\+$'
  for group in a:000
    let code = synIDattr(synIDtrans(hlID(group)), a:attr, fam)
    if code =~? pat
      return code
    endif
  endfor
  return ''
endfunction

let s:ansi = {'black': 30, 'red': 31, 'green': 32, 'yellow': 33, 'blue': 34, 'magenta': 35, 'cyan': 36}

function! s:csi(color, fg)
  let prefix = a:fg ? '38;' : '48;'
  if a:color[0] ==# '#'
    return prefix.'2;'.join(map([a:color[1:2], a:color[3:4], a:color[5:6]], 'str2nr(v:val, 16)'), ';')
  endif
  return prefix.'5;'.a:color
endfunction

function! s:ansi(str, group, default, ...)
  let fg = s:get_color('fg', a:group)
  let bg = s:get_color('bg', a:group)
  let color = (empty(fg) ? s:ansi[a:default] : s:csi(fg, 1)) .
        \ (empty(bg) ? '' : ';'.s:csi(bg, 0))
  return printf("\x1b[%s%sm%s\x1b[m", color, a:0 ? ';1' : '', a:str)
endfunction

" vint: -ProhibitUsingUndeclaredVariable
for s:color_name in keys(s:ansi)
  execute 'function! s:'.s:color_name."(str, ...)\n"
        \ "  return s:ansi(a:str, get(a:, 1, ''), '".s:color_name."')\n"
        \ 'endfunction'
endfor

" END ======> from fzf.vim

fun! rsrchboy#fzf#Plugins(bang) abort " {{{1

    let l:source = []

    for l:plugin in keys(g:plugs)
        let l:info = s:yellow(l:plugin) . "\t"
            \ . s:cyan(g:plugs[l:plugin]['dir']) . "\t"
            \ . s:cyan(g:plugs[l:plugin]['uri'])
        let l:source += [l:info]
    endfor

    let l:sink = 's:tabSecondColSink'

    " crude columiziation enabled via ginormous tabstop
    call fzf#run(fzf#wrap('vim-plug-plugins', {
    \   'source': l:source,
    \   'sink': function(l:sink),
    \   'options': '--ansi --tabstop=30 -m --prompt "vim-plug plugins> "',
    \}))

    return
endfun

fun! s:tabSecondColSink(line) abort
    let l:dir = split(a:line, "\t")[1]
    let l:dir = trim(l:dir, '/', 2) " strip any trailing /
    return rsrchboy#fzf#FindOrOpenTab(l:dir)
endfun

fun! rsrchboy#fzf#Projects(include_remote) abort " {{{1

    let l:source = 'find '
        \ . g:rsrchboy#fzf#project_dirs
        \ . ' '
        \ . g:rsrchboy#fzf#additional_project_dirs
        \ . ' -maxdepth ' . g:rsrchboy#fzf#search_depth
        \ . ' -name .git'
        \ . ' -printf "%h\n"'
    let l:sink = 'rsrchboy#fzf#FindOrOpenTab'

    " e.g. :Projects!
    if a:include_remote
        let l:source = 'sh -c ''(' . l:source . '; cat ~/.vim/repos.txt)'''
        let l:sink = 'rsrchboy#fzf#MaybeClone'
    endif

    call fzf#run(fzf#wrap('projects', {
    \   'source': l:source,
    \   'sink': function(l:sink),
    \   'options': '-m --prompt "Projects> "',
    \}))

    return
endfun

fun! rsrchboy#fzf#MaybeClone(thing) abort " {{{1
    " ...
    if a:thing !~# '^https://.*'
        return rsrchboy#fzf#FindOrOpenTab(a:thing)
    endif

    let l:project = substitute(
                \ substitute(a:thing, '^.*/', '', ''),
                \ '\.git$', '', '',
                \)

    let l:target = $HOME . '/work/' . l:project

    echo 'Hang on, cloning ' . a:thing . ' to ' . l:target
    let l:out = system('git clone ' . a:thing . ' ' . l:target)

    call fzf#run(fzf#wrap('other-repo-git-ls', {
        \   'source': 'git ls-files',
        \   'dir': l:target,
        \   'options': '--prompt "GitFiles in ' . l:target . '> "',
        \   'sink': 'tabe ',
        \}, 0))

    return
endfun

fun! rsrchboy#fzf#FindOrOpenTab(work_dir) abort " {{{1

    " strictly speaking, this isn't really fzf-specific -- but it can live
    " here until we actually use it somewhere else
    "
    " loop over our tabs, looking for one with a t:git_workdir matching our
    " a:workdir; if found, change tab; if not fire up fzf again to find a file
    " to open in the new tab

    " TODO check to see if we've been handed a url to clone.
    "
    " ...or better yet, just make a different command?
    " ...or a ! variant?

    for l:tab in (gettabinfo())
        if get(l:tab.variables, 'git_workdir', '') ==# a:work_dir
            exe 'tabn ' . l:tab.tabnr
            return
        endif
    endfor

    " uggghghhhhhhhhhhhhhhhHHHHHHHHHHHHHH
    let s:work_dir = a:work_dir

    call fzf#run(fzf#wrap('other-repo-git-ls', fzf#vim#with_preview({
        \   'dir': a:work_dir,
        \   'source': 'git -C ' . a:work_dir . ' ls-files --others --exclude-standard --cached',
        \   'options': [ '--prompt', 'GitFiles in ' . a:work_dir . '> ' ],
        \   'sink': function('rsrchboy#fzf#Tabe'),
        \}), 0))

    return
endfun

fun! rsrchboy#fzf#Tabe(file) abort " {{{1
    " Keep fzf from changing the directory back to what it was
    " PPmsg w:fzf_pushd
    let w:fzf_pushd.dir = w:fzf_pushd.origin
    exe 'tabe' s:work_dir . '/' . a:file
endfun

fun! rsrchboy#fzf#Tabs(bang) abort " {{{1
    let l:tabs_list = []

    for l:tab in (gettabinfo())
        let l:line = '[' . l:tab.tabnr . '] '
            \   . get(l:tab.variables, 'tab_page_title', '')
        let l:tabs_list += [ l:line ]
    endfor

    call fzf#run(fzf#wrap('tabs', {
        \   'source': l:tabs_list,
        \   'options': '--prompt "Tabs> "',
        \   'dir': '.',
        \   'sink': function('rsrchboy#fzf#HandleTabs'),
        \}, 0))

    return
endfun

fun! rsrchboy#fzf#HandleTabs(line) abort " {{{1

  let l:tabnr = matchstr(a:line, '\[\zs[0-9]*\ze\]')
  exe 'tabn ' . l:tabnr

  return
endfun

fun! rsrchboy#fzf#Issues() abort " {{{1

    call fzf#run(fzf#wrap('issues', {
        \   'source': 'hub -c color.ui=always -c color.interactive=always issues',
        \   'options': '--ansi --prompt "Issues> "',
        \   'dir': '.',
        \   'sink': function('rsrchboy#fzf#HandleTabs'),
        \}, 0))

    return
endfun

fun! rsrchboy#fzf#ScriptnamesSink(line) abort " {{{1

  " let l:fn = substitute(a:line, "^\d\+:\s+", '', '')
  let l:fn = split(a:line)[1]
  exe 'e ' . l:fn

  return
endfun

function! rsrchboy#fzf#Scriptnames(...) " {{{1
  redir => l:cout
  silent scriptnames
  redir END
  let l:list = split(l:cout, "\n")
  return fzf#run(fzf#wrap('scriptnames', {
  \     'source':  l:list,
  \     'sink':    function('rsrchboy#fzf#ScriptnamesSink'),
  \     'options': '-n2 +m -x --ansi --tiebreak=index --tiebreak=begin --prompt "Scriptnames> "',
  \ }, 0))
endfunction


" vim: foldlevel=0 foldmethod=marker :

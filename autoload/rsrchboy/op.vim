"   ___                       _
"  / _ \ _ __   ___ _ __ __ _| |_ ___  _ __
" | | | | '_ \ / _ \ '__/ _` | __/ _ \| '__|
" | |_| | |_) |  __/ | | (_| | || (_) | |
"  \___/| .__/ \___|_|  \__,_|\__\___/|_|
"       |_|
"  _          _
" | |__   ___| |_ __   ___ _ __ ___
" | '_ \ / _ \ | '_ \ / _ \ '__/ __|
" | | | |  __/ | |_) |  __/ |  \__ \
" |_| |_|\___|_| .__/ \___|_|  |___/
"              |_|
"
" Helper functions for user-defined operators.  (mine, anyways)

fun! rsrchboy#op#Set_op_command(command)
    let s:op_command = a:command
endfun

fun! rsrchboy#op#Op_ThingAndTidy(motion_wiseness)
    " actually do the thing
    execute "'[,']" s:op_command

    " comment it out
    let l:cchar = substitute(&l:commentstring, ' *%s.*', "", "")
    execute "'[,']s/^/" . l:cchar . " /"

    " strip trailing whitespace
    '[,']s/ *$//
endfun

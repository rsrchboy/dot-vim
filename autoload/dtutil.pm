package VIMx::autoload::dtutil;

use v5.10;
use strict;
use warnings;

use VIMx::Symbiont;

# # debugging...
# use Smart::Comments '###';

function args => q{string}, lcfirst => sub {
    return lcfirst $a{string};
};

function args => q{string}, ucfirst => sub {
    return ucfirst $a{string};
};

function args => q{string}, toCamelCase => sub {
    my $s = ucfirst $a{string};
    $s =~ s/_(.)/\u$1/g;
    return $s;
};

function args => q{string}, fromCamelCase => sub {
    my $s = lcfirst $a{string};
    $s =~ s/([A-Z])/_\l$1/g;
    return $s;
};

!!42;

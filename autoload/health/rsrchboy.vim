"     ____ _               _    _   _            _ _   _
"  _ / ___| |__   ___  ___| | _| | | | ___  __ _| | |_| |__
" (_) |   | '_ \ / _ \/ __| |/ / |_| |/ _ \/ _` | | __| '_ \
"  _| |___| | | |  __/ (__|   <|  _  |  __/ (_| | | |_| | | |
" (_)\____|_| |_|\___|\___|_|\_\_| |_|\___|\__,_|_|\__|_| |_|
"
" see https://github.com/rhysd/vim-healthcheck
"
" This file enables
"
"   :CheckHealth rsrchboy
"
" N.B. At the moment, it relies on unpublished patches to vim-plug, and should
" not be considered anything more than an initial playground.

let s:coc = {'impacts': 'coc.nvim'}

let g:health#rsrchboy#binaries = {
    \   'terraform-ls': {
    \       'install-needs': ['go'],
    \       'install': 'go install github.com/hashicorp/terraform-ls@latest',
    \   },
    \  'npm': s:coc,
    \  'node': s:coc,
    \  'git': {},
    \  'git-lfs': {},
    \  'xsel': {},
    \}

function! s:vimPlugSection() abort " {{{1
    call health#report_start('vim-plug plugins!')

    if !exists(':PlugStatus')
        call health#report_warn(':PlugStatus not found; is vim-plug loaded?!')
        return
    endif

    if !exists('g:plugs')
        call health#report_warn('g:plugs is not defined; is vim-plug in use?!')
        return
    endif

    call health#report_warn('this healthcheck does not replace :PlugStatus')

    " https://github.com/junegunn/vim-plug/pull/1223
    if exists('*plug#plugin_info') && exists('*plug#not_loaded')
        let l:loaded = filter(copy(g:plugs), {k, v -> plug#plugin_info(k) })
        let l:not_loaded = plug#not_loaded()
    else
        call health#report_warn(
            \ 'plug#plugin_info() et al are not defined; did you submit that PR yet?!',
            \ 'plugin load/unloaded stats are suspect')
        let l:loaded = filter(copy(g:plugs), {k, v -> !has_key(v, 'uri') })
        let l:not_loaded = filter(copy(g:plugs), {k, v ->  has_key(v, 'uri') })
    endif

    let l:count = len(g:plugs)
    if l:count > 0
        call health#report_ok(l:count . ' plugins registered')
    else
        call health#report_warn('NO plugins registered?!')
    endif


    let l:count = len(l:loaded)
    if l:count > 0
        call health#report_ok(l:count . ' plugins loaded')
    else
        call health#report_warn('NO plugins loaded?!')
    endif

    let l:not_loaded_count = len(l:not_loaded)
    if l:not_loaded_count > 0
        " Strictly speaking, a try/catch block here is a bit overkill.
        " However, this section _has_ acted up before, so it's not going to
        " hurt anything and may help spare some pain for $future_self.
        try
            call health#report_info(l:not_loaded_count . ' plugins not loaded: ' . join(sort(l:not_loaded), ', '))
        catch
            PPmsg l:not_loaded
            echom 'Caught "' . v:exception .. '" in ' .. v:throwpoint
        endtry
    endif

    let l:locals = filter(copy(g:plugs), {k, v -> !has_key(v, 'uri') })
    if len(l:locals) > 0
        call health#report_warn('local copies of plugins found', values(
            \ map(
            \   l:locals,
            \   { k, v -> k . ' @ ' . fnamemodify(v['dir'], ':~:s?/$??') }
            \)))
    else
        call health#report_ok('local plugins: none discovered')
    endif

    return
endfunction

function! s:configSourcedSection() abort " {{{1
    call health#report_start('configuration')
    if len(get(g:, 'health#rsrchboy#optional_sources', [])) == 0
        call health#report_info('No optional source files loaded')
    else
        for l:file in g:health#rsrchboy#optional_sources
            call health#report_warn(l:file . ' sourced')
        endfor
    endif
endfunction

function! s:binariesSection() abort " {{{1
    call health#report_start('binaries')

    for l:bin in sort(keys(g:health#rsrchboy#binaries))
        if !executable(l:bin)
            call health#report_error(l:bin . ' not found!')
        else
            call health#report_ok(l:bin . ' was found at ' . fnamemodify(exepath(l:bin), ':~'))
        endif
    endfor

endfunction

function! health#rsrchboy#check() abort " {{{1
    call s:vimPlugSection()
    call s:binariesSection()
    call s:configSourcedSection()
    return
endfunction

if exists("b:did_indent")
    finish
endif

" TODO may want to investigate vim-go's indent/gohtmltmpl.vim
runtime! indent/markdown.vim

"   __   ___         _      _            _   _
"  / _| / / |_    __| | ___| |_ ___  ___| |_(_) ___  _ __
" | |_ / /| __|  / _` |/ _ \ __/ _ \/ __| __| |/ _ \| '_ \
" |  _/ / | |_  | (_| |  __/ ||  __/ (__| |_| | (_) | | | |
" |_|/_/   \__|  \__,_|\___|\__\___|\___|\__|_|\___/|_| |_|
"
" Custom filetype detection.
"
" We do some extra magic in here, as vim-plug sets the augroup for us, while
" otherwise it is not set.  We detect the group to see if this is going on --
" if no group is set we set it.  This (should) prevent a bunch of orphaned
" autocmds hanging around when this is reloaded.

" detect the current augroup (or lack thereof) through the use of a
" sacrificial autocmd
fun! s:detectAugroup()
    let l:auname = 'BarfyThing' . rand()
    let l:auq = 'au User ' . l:auname

    " vint: -ProhibitAutocmdWithNoGroup
    exe 'au! User' l:auname "echo 'barfing!'"
    redir => l:cout
    silent exe l:auq
    redir END
    exe 'au! User' l:auname

    let l:list = split(l:cout, "\n")

    for l:line in l:list
        if l:line ==# 'User'
            return ''
        endif
        if l:line =~# 'User$'
            let l:parts = split(l:line, ' ')
            return l:parts[0]
        endif
    endfor

    " ummm... shouldn't ever get here!
    return 'WT?!'
endfun

let s:curAuGroup = s:detectAugroup()

if empty(s:curAuGroup)
    augroup filetypedetect
endif

" au!

au BufNewFile,BufRead **/.aws/cli/alias                 setfiletype dosini
au BufNewFile,BufRead **/.aws/config                    setfiletype dosini
au BufNewFile,BufRead **/.aws/credentials               setfiletype dosini
au BufNewFile,BufRead **/.config/systemd/user/**/*.conf setfiletype systemd
au BufNewFile,BufRead **/.config/tmux/*.conf            setfiletype tmux
au BufNewFile,BufRead **/.jira.d/templates/*            setfiletype gotexttmpl
au BufNewFile,BufRead **/.ssh/config.d/*                setfiletype sshconfig
au BufNewFile,BufRead **/dnsmasq.d/*                    setfiletype dnsmasq
au BufNewFile,BufRead **/iptables/*.rules               setfiletype iptables
au BufNewFile,BufRead **/layouts/**/*.html              setfiletype gohtmltmpl   " hugo
au BufNewFile,BufRead **/layouts/partials/**/*.html     setfiletype gohtmltmpl   " hugo
au BufNewFile,BufRead **/offlineimap/config             setfiletype dosini
au BufNewFile,BufRead *.cson                            setfiletype coffee
au BufNewFile,BufRead *.yaml.gotmpl                     setfiletype yaml.gotexttmpl
au BufNewFile,BufRead *.gotmpl                          setfiletype gotexttmpl
au BufNewFile,BufRead *.hcl                             setfiletype terraform
au BufNewFile,BufRead *.j2                              set         ft=jinaj2 " forcibly override
au BufNewFile,BufRead *.mail                            setfiletype mail
au BufNewFile,BufRead *.md.tmpl                         set         ft=gomkdtmpl " forcibly override
au BufNewFile,BufRead *.org                             setfiletype org
au BufNewFile,BufRead *.ovpn                            setfiletype openvpn
au BufNewFile,BufRead *.pa                              setfiletype pulseaudio
au BufNewFile,BufRead *.psgi                            setfiletype perl
au BufNewFile,BufRead *.snippet                         setfiletype snippet
au BufNewFile,BufRead *.snippets                        setfiletype snippets
au BufNewFile,BufRead *.t                               setfiletype perl
au BufNewFile,BufRead *.terminfo                        setfiletype terminfo
au BufNewFile,BufRead *.tt                              setfiletype tt2html
au BufNewFile,BufRead *.tt2                             setfiletype tt2html
au BufNewFile,BufRead *.yaml.gotmpl                     set         ft=yaml.gotexttmpl " forcibly override
au BufNewFile,BufRead *.zsh-theme                       setfiletype zsh
au BufNewFile,BufRead *access.log*                      setfiletype httplog
au BufNewFile,BufRead .config/git/config                setfiletype gitconfig
au BufNewFile,BufRead .config/systemd/user/**/*         setfiletype systemd
au BufNewFile,BufRead .config/yamllint/config           setfiletype yaml
au BufNewFile,BufRead .gitconfig.local                  setfiletype gitconfig
au BufNewFile,BufRead .gitgot*                          setfiletype yaml
au BufNewFile,BufRead .gitpan                           setfiletype yaml
au BufNewFile,BufRead .lfsconfig                        setfiletype gitconfig
au BufNewFile,BufRead .offlineimaprc                    setfiletype dosini
au BufNewFile,BufRead .oh-my-zsh/themes/*               setfiletype zsh
au BufNewFile,BufRead .perlcriticrc                     setfiletype dosini
au BufNewFile,BufRead .saml2aws                         setfiletype dosini
au BufNewFile,BufRead .secrets.baseline                 setfiletype json
au BufNewFile,BufRead .tidyallrc                        setfiletype dosini
au BufNewFile,BufRead .vagrantuser                      setfiletype yaml
au BufNewFile,BufRead .whitesource                      setfiletype json
au BufNewFile,BufRead .yamllint                         set ft=yaml
au BufNewFile,BufRead Changes                           setfiletype changelog
au BufNewFile,BufRead Chart.lock                        setfiletype yaml
au BufNewFile,BufRead NetworkManager.json               setfiletype dosini
au BufNewFile,BufRead Rexfile                           setfiletype perl
au BufNewFile,BufRead alienfile                         setfiletype perl
au BufNewFile,BufRead ansible/**/files/sshkeys/*        setfiletype sshauthkeys
au BufNewFile,BufRead apparmor.d/*                      setfiletype apparmor
au BufNewFile,BufRead attributes/*.rb                   setfiletype ruby.chef
au BufNewFile,BufRead cpanfile                          setfiletype perl
au BufNewFile,BufRead default/grub                      setfiletype sh
au BufNewFile,BufRead fontconfig/config                 setfiletype xml
au BufNewFile,BufRead git-sh-*                          setfiletype sh
au BufNewFile,BufRead gitconfig.local                   setfiletype gitconfig
au BufNewFile,BufRead helmfile.yaml                     set         ft=helm
au BufNewFile,BufRead iptables.rules,ip6tables.rules    setfiletype iptables
au BufNewFile,BufRead lightdm.conf                      setfiletype dosini
au BufNewFile,BufRead offlineimap.conf                  setfiletype dosini
au BufNewFile,BufRead pacman.conf                       setfiletype dosini
au BufNewFile,BufRead pacman.d/*                        setfiletype dosini
au BufNewFile,BufRead partials/**/*.html                setfiletype gohtmltmpl
au BufNewFile,BufRead profanity/accounts                setfiletype dosini
au BufNewFile,BufRead profanity/profrc                  setfiletype dosini
au BufNewFile,BufRead recipes/*.rb                      setfiletype ruby.chef
au BufNewFile,BufRead syslog-ng.conf                    setfiletype syslog-ng
au BufNewFile,BufRead templates/*/*.erb                 setfiletype eruby.chef
au bufnewfile,bufread **/.config/git/config             setfiletype gitconfig
au bufnewfile,bufread **/.config/git/ignore             setfiletype gitignore
au bufnewfile,bufread **/.config/git/includes/*         setfiletype gitconfig
au bufnewfile,bufread **/.config/hub                    setfiletype yaml

" FIXME commenting this out, as vim-github-hub should set this for us
" " the 'hub' tool creates a number of comment files formatted in the same way
" " as a git commit message.
" autocmd BufEnter *.git/**/*_EDITMSG set ft=gitcommit

" heuristic for ssh public key files (ssh_authorized_keys)
" ...yes, this is hardly comprehensive (e.g. command= will botch it)
au BufNewFile,BufRead *
    \ if getline(1) =~ '^\(ssh-\(rsa\|dsa\)\|ecdsa-sha2-nistp256\)' |
    \   setf ssh_authorized_keys |
    \ endif

if empty(s:curAuGroup)
    augroup END
endif

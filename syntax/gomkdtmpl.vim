" small syntax for handling markdown template files.

syntax clear

" last in wins!
runtime! syntax/markdown.vim
unlet! b:current_syntax
runtime! syntax/gotexttmpl.vim
unlet! b:current_syntax

" carry-over from gotexttmpl.vim -- will investigate if something breaks, but
" mkd also leverages html syntax, so...
syn cluster htmlPreproc add=gotplAction,goTplComment

let b:current_syntax = "gomkdtmpl"

#!/usr/bin/env make
#
# a couple targets to assist in maintaining my vim setup
#
# Chris Weyl <cweyl@alumni.drew.edu> 2017

.PHONY: bootstrap commit-spellings unshallow help

help:
	# ZOMG!  It has been 6 months; what does this do again?
	#
	# vim-plug: update the git-subtree included copy of vim-plug
	# spellings: yeah, that

vim-plug:
	git stash
	git -c log.showSignature=false subtree pull --prefix=bootstrap/vim-plug --squash https://github.com/junegunn/vim-plug.git master
	git stash pop

spellings:
	git commit -m 'Spellings!' spell/

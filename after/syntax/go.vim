" vim-go syntax overrides

" Override to cover _all_ of the package comment; e.g. everything before the
" opening package stanza... but don't touch any build directive!
"
" This keeps, e.g., distinct license headers and package comment blocks from
" only having the last highlighted.  There's room for improvement, e.g.
" TODO error if more than two stanzas -- though this is debatable
"
if !exists('*go#config#HighlightBuildConstraints')
  " if we're here, then vim-go has yet to be loaded; there's no point
  " continuing.
  finish
endif

if go#config#HighlightBuildConstraints() || go#config#FoldEnable('package_comment')
  syn clear goPackageComment
  exe 'syn region  goPackageComment    start=/\v((^\/\/.*\n)|^\s*\/\*.*\n(.*\n)*\s*\*\/\n|\n)+\s*package/'
        \ . ' end=/\v\n\s*package/he=e-7,me=e-7,re=e-7'
        \ . ' contains=@goCommentGroup,@Spell'
        \ . ' nextgroup=goPackageComment,goPackage skipwhite skipempty skipnl'
        \ . (go#config#FoldEnable('package_comment') ? ' fold' : '')
  " hi def link goPackageComment    Comment
endif

" Build Constraints
if go#config#HighlightBuildConstraints()
  syn clear goBuildComment
  syn region  goBuildComment      matchgroup=goBuildCommentStart
        \ start="//\s*+build\s"rs=s+2 end="$"
        \ contains=goBuildKeyword,goBuildDirectives
        \ nextgroup=goPackageComment,goPackage
endif

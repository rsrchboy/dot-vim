" upstreamish
syntax region shLine matchgroup=dockerfileKeyword start=/\v^\s*%(ONBUILD\s+)?%(RUN|CMD|ENTRYPOINT)\s/ end=/\v$/ contains=@SH

syn match   dockerfileTodo /\<\(NOTES\?\|TBD\|FIXME\|XXX\|PLAN\|TODO\)[:]\?/ containedin=dockerfileComment contains=NONE,@NoSpell

hi def link dockerfileTodo Todo

" everyone loves a todo!
syn match yamlTodo /\<\(NOTES\?\|TBD\|TODO\|FIXME\|XXX\|PLAN\)[:]\?/
            \ contained contains=NONE,@NoSpell
            \ containedin=yamlComment

hi def link yamlTodo Todo

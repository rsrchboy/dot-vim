"  ____           _ ____
" |  _ \ ___ _ __| | ___|
" | |_) / _ \ '__| |___ \
" |  __/  __/ |  | |___) |
" |_|   \___|_|  |_|____/
"

if !has_key(g:, 'perl_no_sql') " {{{
    let s:syn = b:current_syntax
    unlet b:current_syntax
    syntax include @SQL syntax/sql.vim
    let b:current_syntax = s:syn
    unlet s:syn
    syntax region perlHereDocSQL start=+<<['"]SQL['"].*;\s*$+ matchgroup=perlStringStartEnd end=+^SQL$+ contains=@SQL

    " indented!  req v5.26
    syntax region perlHereDocSQL start=/<<\~['"]SQL['"].*;\s*$/ matchgroup=perlStringStartEnd end=/^\s*SQL$/ contains=@SQL

    " Helps the heredoc be recognized regardless of where it's initiated
    syn cluster perlExpr add=perlHereDocSQL
endif " }}}

"  _                                    _
" | | _____ _   ___      _____  _ __ __| |___
" | |/ / _ \ | | \ \ /\ / / _ \| '__/ _` / __|
" |   <  __/ |_| |\ V  V / (_) | | | (_| \__ \
" |_|\_\___|\__, | \_/\_/ \___/|_|  \__,_|___/
"           |___/

" for syntax purposes, our only legal characters for keywords are a-z
" ...so things like 'IF' won't show up highlighted like 'if'
syn iskeyword 97-122,_

syn keyword perlKeywordMooseish with extends has after before around inner
hi def link perlKeywordMooseish Keyword

syn clear   perlConditional " {{{1
syn keyword perlConditional if elsif unless given when default
syn match   perlConditional           "\<else\%(\%(\_s\*if\>\)\|\>\)" contains=perlElseIfError skipwhite skipnl skipempty

syn clear perlControl " {{{1
syn match perlControl "^\%(BEGIN\|CHECK\|INIT\|END\|UNITCHECK\)\ze\(\_s\|{\)\+" skipwhite skipnl
syn clear perlHereDocStart

syn clear perlRepeat " {{{1
syn keyword perlRepeat while for foreach do until continue

syn clear perlStatementFlow " {{{1
syn keyword perlStatementFlow caller die dump eval exit wantarray evalbytes
" from Try::Tiny
syn keyword perlStatementFlow try catch finally

syn clear perlStatementList " {{{1
syn keyword perlStatementList splice unshift shift push pop join reverse grep map sort unpack
" in List::Util
syn keyword perlStatementList apply reduce any all none notall first max maxstr min minstr product sum sum0 pairs unpairs pairkeys pairvalues pairfirst pairgrep pairmap shuffle uniq uniqnum uniqstr

syn clear perlStatementNumeric " {{{1
syn keyword perlStatementNumeric abs atan2 cos exp hex int log oct rand sin sqrt srand

syn clear perlStatementNetwork " {{{1
syn keyword perlStatementNetwork
    \ endhostent endnetent endprotoent endservent
    \ gethostent getnetent getprotoent getservent
    \ sethostent setnetent setprotoent setservent
    \ gethostbyaddr gethostbyname getnetbyaddr getnetbyname
    \ getprotobyname getprotobynumber
    \ getservbyname getservbyport

syn clear perlStatementFiledesc " {{{1
syn keyword perlStatementFiledesc
    \ nextgroup=perlFiledescStatementNocomma skipwhite
    \ binmode close closedir eof fileno getc lstat print printf readdir
    \ readline readpipe rewinddir say select stat tell telldir write
syn clear perlFiledescStatementNocomma
syn match perlFiledescStatementNocomma "\u\w*\ze\_s\+" transparent contained contains=perlFiledescStatement
syn clear perlFiledescStatement
syn match perlFiledescStatement "\w*" contained
syn keyword perlStatementFiledesc
    \ nextgroup=perlFiledescStatementComma skipwhite
    \ fcntl flock ioctl open opendir read seek seekdir sysopen sysread sysseek
    \ syswrite truncate

syn clear perlStatementFiles " {{{1
syn keyword perlStatementFiles chdir chmod chown chroot glob link mkdir readlink rename rmdir symlink umask unlink utime

" these are perlStatementFiles in vim-perl, but they're really more along the
" lines of an operator, so...
syn match perlOperatorFiles "-[rwxoRWXOezsfdlpSbctugkTBMAC]\>"

syn clear perlStatementHash " {{{1
syn keyword perlStatementHash delete each exists keys values

syn clear perlStatementInclude " {{{1
syn keyword perlStatementInclude skipwhite nextgroup=perlUsedPkg require import unimport use no
" syn match   perlStatementInclude "\<\%(use\|no\)\s\+\%(\%(attributes\|attrs\|autodie\|autouse\|parent\|base\|big\%(int\|num\|rat\)\|blib\|bytes\|charnames\|constant\|diagnostics\|encoding\%(::warnings\)\=\|feature\|fields\|filetest\|if\|integer\|less\|lib\|locale\|mro\|open\|ops\|overload\|overloading\|re\|sigtrap\|sort\|strict\|subs\|threads\%(::shared\)\=\|utf8\|vars\|version\|vmsish\|warnings\%(::register\)\=\)\>\)\="

syn clear perlStatementIOfunc " {{{1
syn keyword perlStatementIOfunc syscall dbmopen dbmclose

syn clear perlStatementIPC " {{{1
syn keyword perlStatementIPC
    \ msgctl msgget msgrcv msgsnd
    \ semctl semget semop
    \ shmctl shmget shmread shmwrite

syn clear perlStatementMisc " {{{1
syn keyword perlStatementMisc warn format formline reset scalar prototype lock tie tied untie

syn clear perlStatementProc " {{{1
syn keyword perlStatementProc alarm exec fork getpgrp getppid getpriority kill pipe setpgrp setpriority sleep system times wait waitpid

syn clear perlStatementPword " {{{1
syn keyword perlStatementPword getpwuid getpwname getgrgid getgrnam getlogin endgetpwent endsetpwent endgetgrent endsetgrent

syn clear perlStatementRegexp " {{{1
syn keyword perlStatementRegexp skipwhite nextgroup=perlNakedQR pos quotemeta split study

syn clear perlStatementScalar " {{{1
syn keyword perlStatementScalar chomp chop chr crypt index rindex lc lcfirst length ord pack sprintf substr fc uc ucfirst

syn clear perlStatementSocket " {{{1
syn keyword perlStatementSocket accept bind connect getpeername getsockname getsockopt listen recv send setsockopt shutdown socket socketpair

syn clear perlStatementStorage " {{{1
syn keyword perlStatementStorage my local our state

syn clear perlStatementTime " {{{1
syn keyword perlStatementTime gmtime localtime time

syn clear perlString " {{{1
" this was...  slow
" The => operator forces a bareword to the left of it to be interpreted as
" a string
syn match  perlString "-\?\I\i*\%(\_s*=>\)\@="
" same -- recreating
syn match  perlString   "\<\%(v\d\+\%(\.\d\+\)*\|\d\+\%(\.\d\+\)\{2,}\)\>" contains=perlVStringV
syn region perlString   matchgroup=perlStringStartEnd start=+"+  end=+"+ contains=@perlInterpDQ keepend extend

syn clear perlStatementControl " {{{1
syn keyword perlStatementControl return break
syn keyword perlStatementControl skipwhite nextgroup=perlLabel last next redo goto

syn clear perlStatementVector " {{{1
syn keyword perlStatementVector vec

"                   _
"  _ __   __ _  ___| | ____ _  __ _  ___  ___
" | '_ \ / _` |/ __| |/ / _` |/ _` |/ _ \/ __|
" | |_) | (_| | (__|   < (_| | (_| |  __/\__ \
" | .__/ \__,_|\___|_|\_\__,_|\__, |\___||___/
" |_|                         |___/

syn clear perlPackageDecl
syn clear perlStatementPackage
syn keyword perlStatementPackage skipwhite next=perlPackageName package
syn match   perlPackageName "\%(\h\|::\)\%(\w\|::\)*" contained

" }}}1

" Str matches "tr" with syn iskw set to all lower-case
" syn region perlMatch	matchgroup=perlMatchStartEnd start=+\<\%(::\|'\|->\)\@<!\%(tr\|y\)\>\s*\z([^[:space:]([{<#]\)+ end=+\z1+me=e-1 contains=@perlInterpSQ nextgroup=perlTranslationGQ
" syn region perlMatch	matchgroup=perlMatchStartEnd start=+\<\%(::\|'\|->\|\_s\+\)\@<!\%(tr\|y\)\>\s*\z([^[:space:]([{<#]\)+ end=+\z1+me=e-1 contains=@perlInterpSQ nextgroup=perlTranslationGQ
" syn region perlMatch	matchgroup=perlMatchStartEnd start=+\<\%(::\|'\|->\|\_s\+\)\@<=\%(tr\|y\)\>\s*\z([^[:space:]([{<#]\)+ end=+\z1+me=e-1 contains=@perlInterpSQ nextgroup=perlTranslationGQ

" syn region perlQQ             matchgroup=perlStringStartEnd start=+\<\%(::\|'\|->\)\@<!q\>\s*\z([^[:space:]#([{<]\)+ end=+\z1+ contains=@perlInterpSQ keepend extend

" e.g.: next LABEL;
" this is a bit convoluted -- \< and \> use iskeyword, but we've
" constrained that (for syntax purposes) to be lowercase.
syn match perlLabel /\_s\+\zs\u\+\ze\(\_s\|;\)/ contained


" this appears to work, but generates a _lot_ of additional perlBracesSQ
" matches with multiple embedded.
syn clear  perlBracesSQ
syn region perlBracesSQ         start=+{+ end=+}+ contained contains=@perlInterpSQ,perlBracesSQ
syn region perlQQ               matchgroup=perlStringStartEnd start=+\<\%(::\|'\|->\)\@<!q\>\s*\z([^[:space:]#([{<]\)+ end=+\z1+ contains=@perlInterpSQ
syn clear  perlBracesDQ
syn region perlBracesDQ         start=+{+ end=+}+ contained contains=perlBracesDQ,@perlInterpDQ

" $+ isn't picked up by perlVarPlain otherwise -- '+' is (correctly) not in
" isident or iskeyword
syn match  perlVarPlain "\$+" skipwhite nextgroup=perlHashSlot,perlVarMember,perlVarSimpleMember,perlMethod,perlPostDeref

" just for the nextgroup addition
syn match  perlVarPlain       "\%([@$]\|\$#\)\$*\%(\I\i*\)\=\%(\%(::\|'\)\I\i*\)*\%(::\|\i\@<=\)" contains=perlPackageRef skipwhite nextgroup=perlHashSlot,perlVarMember,perlVarSimpleMember,perlPostDeref,perlDerefOrInvoke

syn clear perlOperator " {{{1
syn keyword perlOperator  undef defined eq ne ge gt le lt cmp not and or xor not bless ref do
" must not match /^=/ because POD
syn match perlOperator           /\%(^\)\@!=/

" FIXME include code?
syn match perlDeref    /\\\ze[$@%]/ nextgroup=perlVarPlain

" <=>, =>, //, //=, ||, ||=
syn match perlOperator '\%(<\==>\|//=\=\|||=\=\)'
" *, *=, ., .=, +, +=, -, -=, etc
syn match perlOperator #[*.+-/]=\=#
" |, ||, etc
syn match perlOperator /[|&>+-]\{1,2}/
syn match perlOperator /[?:^]/
" Handle << carefully, for heredocs
syn match perlOperator /<\ze[^<]/

syn match perlOperatorLike /[=!]\~/ nextgroup=perlNakedQR,perlMatchRegex skipwhite
syn region perlNakedQR matchgroup=perlStringStartEnd start=+\z([/!]\)+  end=+\z1[imosxdual]*+
            \ contains=@perlInterpMatch keepend extend contained skipwhite oneline
syn region perlMatchRegex matchgroup=perlStringStartEnd start=+m{+  end=+}[imosxdual]*+
            \ contains=@perlInterpMatch keepend extend contained skipwhite oneline


" allow lines starting with ')->' to be matched, but don't match the ')'
syn match perlDerefOrInvoke     /->/
            \ nextgroup=perlFuncName,perlVarAsMethod,perlHashSlot,perlPostDeref,perlArraySlot
            \ skipwhite skipnl
syn match perlFuncName          +\W\%(\h\|::\)\%(\w\|::\|'\w\)*\W+
            \ contained
            \ contains=perlPackageRef,perlPackageDelimError
            \ nextgroup=perlMethodArgs,perlFuncName,perlDerefOrInvoke
            \ skipwhite

" " This is kinda a mess -- perlQQ won't match if we match 'qw' first,
syn match perlPkgOrFunc         +\I\%(\i\|::\)*\ze\_s*\%(->\|(\)+
            \ contains=perlPackageRef,perlPackageDelimError
            \ nextgroup=perlDerefOrInvoke
syn match perlVarAsMethod       /\$\w\+/
            \ contained
            \ nextgroup=perlMethodArgs
syn match perlPackageDelimError /'/
            \ contained

syn cluster perlPkgDelimThings contains=perlPlainVar,perlSubName,perlPackageDecl,perlPackageName
syn match perlPkgDelim /::/ contained containedin=@perlPkgDelimThings

syn keyword perlOperator nextgroup=perlQQNaked skipwhite qw
syn region perlQQNaked matchgroup=perlStringStartEnd start=+(+ end=+)+ contains=@perlInterpSQ,perlParensSQ keepend extend skipwhite contained
syn region perlQQNaked matchgroup=perlStringStartEnd start=+{+ end=+}+ contains=@perlInterpSQ,perlParensSQ keepend extend skipwhite contained

" mine
syn clear   perlSubName
syn match   perlSubName  "\%(\h\|::\|'\w\)\%(\w\|::\|'\w\)*" contained extend skipwhite
syn clear   perlFunction
syn keyword perlFunction nextgroup=perlSubName skipwhite sub
syn clear   perlSubDeclaration

" fixes qr{}'s where the highlighting doesn't close; e.g. this line
" from Statocles::App::Blog:
"       my $is_dated_path = qr{^$root/?(\d{4})/(\d{2})/(\d{2})/};
syn match perlSpecialString /\\[dDXvVhHRKsSwW]\%({\d\+\%(,\d*\)\=}\)\=/ contained extend

" NOTE these bits will only really work if we have g:perl_no_extended_vars
" set, as we're basically replacing it here.

syn match perl__ /__\%(FILE\|LINE\)__/

" this was messing up highlighting after something like: $+{...}
syn cluster perlExpr remove=perlBraces

" TODO cluster for perlOperator (if we split qw out for use)

syntax region perlDATA matchgroup=PreProc start="^__DATA__$" skip="." end="." contains=@perlDATA

if get(g:, 'perl_no_extended_vars', 1) " {{{1

    " this seems somewhat borked, and interferes with our extended_vars stuff
    syn clear perlStatementIndirObjWrap
    syn cluster perlExpr remove=perlStatementIndirObjWrap

    syn cluster perlExpr add=perlString,perlVarPlain,perlVarSimpleMemberName,perlDerefOrInvoke
    syn region perlHashSlot matchgroup=Delimiter start="{" skip="\\}" end="}" contained contains=@perlExpr nextgroup=perlVarMember,perlVarSimpleMember,perlPostDeref,perlDerefOrInvoke,perlHashSlot extend
    syn match  perlHashSlot "{\s*\I\i*\s*}" nextgroup=perlVarMember,
        \perlVarSimpleMember,perlPostDeref,perlDerefOrInvoke,perlHashSlot
        \ contains=perlVarSimpleMemberName,perlHashSlotDelimiters
        \ contained extend
    " syn region perlArraySlot matchgroup=Delimiter start=/\[/ end=/\]/ contained
    syn match perlArraySlot /\[\d\+\]/ contained contains=perlDelimiters,perlNumber

    syn match  perlHashSlotDelimiters /[{}]/ contained
    syn match  perlVarSimpleMemberName  "\I\i*" contained

    " XXX after perlVarPlain nextgroup=perlHashSlot

    syn match perlBlockDerefOps /[@$%]/ contained
    syn match perlBlockDeref    /[@$%]{/ contains=perlBlockDerefOps

    syn match  perlDelimiters /[{}[\])]/ skipwhite
    " e.g.: if (/.../)
    syn match  perlDelimiters /(/ skipwhite

    syn match perlPackageConst  "__PACKAGE__" nextgroup=perlPostDeref,perlDerefOrInvoke
    syn match perlSubConst      "__SUB__" nextgroup=perlPostDeref,perlDerefOrInvoke
    syn match perlPostDeref     "\%($#\|[$@%&*]\)\*" contained
    syn region  perlPostDeref   start="\%($#\|[$@%&*]\)\[" skip="\\]" end="]" contained contains=@perlExpr nextgroup=perlVarSimpleMember,perlVarMember,perlPostDeref
    " syn region  perlPostDeref matchgroup=perlPostDeref start="\%($#\|[$@%&*]\){" skip="\\}" end="}" contained contains=@perlExpr nextgroup=perlVarSimpleMember,perlVarMember,perlPostDeref
endif " }}}1

" syn region perlQQ matchgroup=perlStringStartEnd start=+/+  end=+/[imosxdual]*+ contains=@perlInterpSlash keepend extend contained
" syn region perlQQ matchgroup=perlStringStartEnd start=+/+  end=+/[imosxdual]*+ contains=@perlInterpSlash contained

hi def link perlKeyword Keyword

hi link perlBlockDeref         Delimiter
hi link perlBlockDerefOps      perlOperator
hi link perlBraces             Delimiter
hi link perlDelimiters         Delimiter
hi link perlDeref              Operator
hi link perlDerefOrInvoke      Operator
hi link perlFuncName           Function
hi link perlHashSlotDelimiters Delimiter
hi link perlNakedQR            perlQQ
hi link perlOperatorFiles      perlOperator
hi link perlOperatorLike       perlOperator
hi link perlPackageConst       Macro
hi link perlPackageDelimError  Error
hi link perlPkgDelim           Delimiter
hi link perlPkgOrFunc          Function
hi link perlPostDeref          Operator
hi link perlQQNaked            perlString
hi link perlSubConst           Macro
hi link perlVarAsMethod        Identifier
hi link perl__                 Macro

syn match perlLineDelimiter /;$/
hi link perlLineDelimiter Delimiter

if !get(g:, 'perl_use_region', 0) " {{{

    " syn keyword perlIncludeUse    nextgroup=perlIncludeUsedPkg containedin= use
    syn keyword perlUse         skipwhite nextgroup=perlUsedPkg,perlUsedPerlVer use
    syn match   perlUsedPkg     +\%(\h\|::\|'\w\)\%(\w\|::\|'\w\)*\ze\%(;\|\_s\+\)+
        \ contained skipwhite nextgroup=perlQQ contains=perlPackageDelimError
    syn match   perlUsedPerlVer /\%(v5.\d\d\=\|5.\d\d\d\)/      contained

    hi link perlUse         Include
    hi link perlUsedPkg     perlType
    hi link perlUsedPerlVer PreCondit

else " {{{

    " this is experimental, unfinished, and more of itch-scratching, really.
    syn region perlUseRegion matchgroup=perlStatementUse start=/^\s*use\>/ matchgroup=Delimiter end=/;/

    hi link perlStatementUse Include
endif " }}}1

syn match perlOperator           "\<\%(blessed\)\>"

syn keyword perlStatement with requires

syn match perlSubRef   /\\&\I\i\+/ contains=perlOperator
syn match perlOperator /\\&/ nextgroup=perlFuncName

hi link perlStatementMethodModifier perlStatement
hi link perlMethodModifier          perlSubName
hi link perlAttribute               perlSubName
hi link perlSubRef                  perlSubName
hi link perlMMError                 Error

" "normal"
syn match   perlTodo /\<\(NOTES\?\|TBD\|FIXME\|XXX\|PLAN\)[:]\?/ contained contains=NONE,@NoSpell

syn match perlPodWeaverSpecialComment "\C^# \zs[A-Z0-9]\+: " contained containedin=perlComment
syn match perlCriticOverride          /## \?\(no\|use\) critic.*$/ containedin=perlComment contained
syn match perlTidyOverride            /#\(<<<\|>>>\).*$/ containedin=perlComment contained

" don't do spell checking in smart comments
syn region perlSmartComment start=/###/ end=/$/ oneline

hi link perlCriticOverride          Ignore
hi link perlTidyOverride            Ignore
hi link perlPodWeaverSpecialComment SpecialComment
hi link perlSmartComment            perlComment

" __END__

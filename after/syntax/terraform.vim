"  _                       __                         ___          _
" | |_ ___ _ __ _ __ __ _ / _| ___  _ __ _ __ ___    / / |__   ___| |
" | __/ _ \ '__| '__/ _` | |_ / _ \| '__| '_ ` _ \  / /| '_ \ / __| |
" | ||  __/ |  | | | (_| |  _| (_) | |  | | | | | |/ / | | | | (__| |
"  \__\___|_|  |_|  \__,_|_|  \___/|_|  |_| |_| |_/_/  |_| |_|\___|_|
"
" Add YAML and JSON syntax highlighting to appropriately named heredocs.
"
" Terragrunt, too!
"
" The ending highlighing is a bit... off.  Fix that before pushing upstream.

if exists('b:current_syntax')
    let s:syn = b:current_syntax
    unlet b:current_syntax
endif

syntax include @JSON syntax/json.vim
unlet b:current_syntax
syntax include @YAML syntax/yaml.vim
unlet b:current_syntax
syntax include @SH syntax/sh.vim

if exists('s:syn')
    let b:current_syntax = s:syn
    unlet s:syn
else
    unlet b:current_syntax
endif

syn cluster terraBlockNotContains contains=terraBlockType,terraSplat

try
    " enable block folding
    syn clear terraBlock
    syn region terraBlock matchgroup=terraBraces start="{" end="}" fold transparent contains=ALLBUT,@terraBlockNotContains
    syn match terraNamedBlock /\<\k\+\ze\s*{/ nextgroup=terraBlock
catch
    if stridx(v:exception, ':E28:') < 0
        echoerr v:exception
    endif
endtry

syn match terraOperator /=/
syn match terraSplat /\*/ contained

syn match terraAttribute /\<\k\+\ze\s*=/

" terraform additions
syn keyword terraBlockType provisioner
" terragrunt additions
syn keyword terraBlockType dependency generate extra_arguments before_hook after_hook

" containedin but not contained: everywhere including this other place
syn keyword terraFunc containedin=terraStringInterp
            \ find_in_parent_folders
            \ get_aws_account_id
            \ get_aws_caller_identity_arn
            \ get_env
            \ get_parent_terragrunt_dir
            \ get_platform
            \ get_terraform_command
            \ get_terragrunt_dir
            \ path_relative_from_include
            \ path_relative_to_include
            \ read_terragrunt_config
            \ run_cmd
            \ sops_decrypt_file

hi link terraFunc Function

" finish

syn match terraVar /\<\%(each\|local\|module\|var\|data\|path\)\%(\.\k\+\)\+\>/ skipwhite nextgroup=terraVarIndex containedin=terraStringInterp
syn match terraVar /\<\k\+\%(\.\k\+\)\{2,}\>/ skipwhite nextgroup=terraVarIndex containedin=terraStringInterp
syn match terraVar /\<\k\+\%(\.\k\+\)\{1,2}\.\*\.\k\+\>/ skipwhite nextgroup=terraVarIndex contains=terraSplat containedin=terraStringInterp
syn match terraDot /\./ contained containedin=terraVar

" FIXME WIP
syn match terraVarIndex "\[\d\+\]" skipwhite contained

" everyone loves a todo!
syn match terraTodo /\<\(NOTES\?\|TBD\|TODO\|FIXME\|XXX\|PLAN\)[:]\?/
            \ contained contains=NONE,@NoSpell
            \ containedin=terraComment

hi def link terraTodo Todo

syn clear  terraStringInterp
syn region terraStringInterp  matchgroup=terraBraces start=/[^$]\zs\${/ end=/}/ contains=terraValueFunction,terraValueVarSubscript,terraStringInterp contained


" syn clear terraHereDocText
syn region terraHereDocJSON matchgroup=terraHereDocText start=/<<JSON/ end=/^JSON/ keepend contains=terraStringInterp,@JSON
syn region terraHereDocYAML matchgroup=terraHereDocText start=/<<YAML/ end=/^YAML/ keepend contains=terraStringInterp,@YAML
syn region terraHereDocSH   matchgroup=terraHereDocText start=/<<SH/   end=/^SH/   keepend contains=terraStringInterp,@SH

" Make fold markers easy to ignore.
"
" n.b. there ought to be a way to make this fairly generic

syn match terraFoldMarkerOpen    /\(# \)\?{{{\(\d\+\)\?/ contained containedin=terraComment
syn match terraFoldMarkerOpen          /# {{{\(\d\+\)\?/ contained containedin=terraComment
syn match terraFoldMarkerClose   /\(# \)\?}}}\(\d\+\)\?/ contained containedin=terraComment
syn match terraFoldMarkerClose         /# }}}\(\d\+\)\?/ contained containedin=terraComment

hi link terraFoldMarkerOpen  Ignore
hi link terraFoldMarkerClose Ignore

hi link terraDot        Delimiter
hi link terraVar        Identifier
hi link terraOperator   Operator
hi link terraAttribute  String
hi link terraNamedBlock Structure
hi link terraSplat      Repeat

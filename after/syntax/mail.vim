" A couple mail syntax tweaks
"
" Author: Chris Weyl <cweyl@alumni.drew.edu>

" turn off spell checking in mail sigs -- note the addition of ",@NoSpell" is
" the only difference from the stock line...  this makes me wish I knew some
" way to just append to a syntax region's "contains" list
syn region mailSignature keepend contains=@mailLinks,@mailQuoteExps,@NoSpell start="^--\s$" end="^$" end="^\(> \?\)\+"me=s-1 fold

syn match   mailTodo /\<\(FIXME\|XXX\)\>/ contains=NONE,@NoSpell
hi def link mailTodo Todo

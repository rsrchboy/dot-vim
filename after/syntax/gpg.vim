
syn keyword gpgOption   contained skipwhite nextgroup=gpgArg
            \   trust-model auto-key-locate

syn keyword gpgOption   contained skipwhite nextgroup=gpgArgError
            \   auto-key-retrieve no-comments no-emit-version
            \   require-cross-certification

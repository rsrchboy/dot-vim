" a couple additional settings for markdown type buffers

let s:tools = {}
execute g:rsrchboy#buffer#tools.ftplugin_guard('markdown')

call s:tools.spell_for('markdown')

" call s:tools.inoremap('--', '—')

call s:tools.surround('L', "[](\r)")

call s:tools.setl('conceallevel', 2)
call s:tools.setl('softtabstop',  4)
call s:tools.setl('shiftwidth',   4)

" so to prevent wonkiness
call s:tools.setl('comments',    'b:>')

call s:tools.llnnore2map('tf', ':TableFormat<CR>')

" 3 -> "```\n ... \n```"
" S -> "```sh\n ... \n```"
let b:surround_51 = "```\n\r\n```"
let b:surround_83 = "```sh\n\r\n```"

unlet s:tools

" __END__

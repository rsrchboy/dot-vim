" Additional setup for vim files
"
let s:tools = {}
execute g:rsrchboy#buffer#tools.ftplugin_guard('vim')

" Section: settings {{{1

call s:tools.spell_for('vim')

" oddly, this doesn't seem to be set...
let &l:commentstring = '" %s'
call s:tools.setl('foldmethod', 'marker')
" call s:tools.setplus('iskeyword', ':')

" vim-scriptease claims that : in iskeyword 'breaks tags, among other things'.
" I'm finding the exact opposite -- that it's not even included in there by
" default, either.  Soo...  yeah, I'm adding it.
setlocal iskeyword+=:

" Section: mappings {{{1

" local mappings
nnoremap <buffer> <silent> ,l :Tabularize /let<CR>

" fold markers w/o regard to comments
call s:tools.llnnoremap('fO', ':<C-U>execute ''s/\s*\({{{\d*\)*\s*$/ {{{''.v:count.''/''')
call s:tools.llnnoremap('fC', ':<C-U>execute ''s/\s*\(}}}\d*\)*\s*$/ }}}''.v:count.''/''')

" fold markers 'adding' comments
call s:tools.llnnoremap('fo', ':<C-U>execute ''s/\s*\(\("\s*\)\?{{{\d*\)*\s*$/ " {{{''.v:count.''/''')
call s:tools.llnnoremap('fc', ':<C-U>execute ''s/\s*\(}}}\d*\)*\s*$/ " }}}''.v:count.''/''')

call s:tools.llnnoremap('ax', ':s/\s*[.,;]*\s*$//')
call s:tools.llnnoremap('a)', ':s/\s*[.,;]*\s*$/)/')

call s:tools.surround('q', "''\r''")
let b:surround_64  = "@{ \r }"
let b:surround_37  = "%{ \r }"

" disable folding, as it tends to get... wonky here
" vim: set foldmethod=manual foldcolumn=0 :

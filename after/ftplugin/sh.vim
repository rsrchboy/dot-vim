" Additional setup for sh files

let s:tools = {}
execute g:rsrchboy#buffer#tools.ftplugin_guard('sh')

call s:tools.setplus('formatoptions', 'ro')
call s:tools.setminus('formatoptions', 't')
call s:tools.setl('include', '^\s*source\>')

" local mappings
nnoremap <buffer> <silent> ,;; :Tabularize /;;<CR>

call s:tools.llnnoremap('fo', ':<C-U>execute ''s/\s*\({{{\d*\)*\s*$/ {{{''.(v:count ? v:count : 1).''/''')
call s:tools.llnnoremap('fc', ':<C-U>execute ''s/\s*\(}}}\d*\)*\s*$/ }}}''.(v:count ? v:count : 1).''/''')

" vim-surround mappings
" D -> [[ ... ]]
" let b:surround_68 = "[[ \r ]]"

call rsrchboy#buffer#shellSurrounds()

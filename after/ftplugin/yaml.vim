setl sw=2

" '-' is legal in, e.g., anchors
setl iskeyword+=-

if fnamemodify(expand("%"), ':t') ==# '.gitlab-ci.yml'
    " setlocal spell
    " autocmd InsertEnter <buffer> setlocal spell
    " autocmd InsertLeave <buffer> setlocal nospell
    let b:gitlab_ci = 1
    " finish
endif

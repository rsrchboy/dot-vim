" a couple additional settings for dosini type buffers

let s:tools = {}
execute g:rsrchboy#buffer#tools.ftplugin_guard('dosini')

if fnamemodify(@%, ':t') == 'pacman.conf'

    " yeah, it's not quite dosini
    call s:tools.setl('commentstring', '#%s')
endif

setl tabstop=4

unlet s:tools
" __END__

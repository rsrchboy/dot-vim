" Additional setup for squid files

let s:tools = {}
execute g:rsrchboy#buffer#tools.ftplugin_guard('squid')

call s:tools.setplus('formatoptions', 'ro')
call s:tools.setminus('formatoptions', 't')

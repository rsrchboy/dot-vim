" let &l:path = go#path#Detect() . '/src'

call rsrchboy#buffer#SetSpellOpts('go')

" auto-run our fixers
let b:ale_fix_on_save = 1

" various tweaks to make the whole tabs thing more tolerable
setl shiftwidth=4
setl sts=0
setl ts=4
setl foldmethod=syntax

" for some reason, vim-go sets fo=cqj... or at least -=t
set formatoptions+=roq

" start with all folds closed -- and right now the only folding we support is
" package-level comments
setl foldlevel=0

nnoremap <buffer> <localleader>gty :GoAddTags yaml<CR>
vnoremap <buffer> <localleader>gty :GoAddTags yaml<CR>

map <buffer> <localleader>gE <Plug>(go-iferr)

" nnoremap <buffer> <silent> q :q<CR>

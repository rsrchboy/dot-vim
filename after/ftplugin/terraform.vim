" a couple additional settings for terraform type buffers

let s:tools = {}
execute g:rsrchboy#buffer#tools.ftplugin_guard('terraform')

" call s:tools.setno('wrap')
" call s:tools.setplus('iskeyword', '-')

" we need to set this, otherwise the JSON plugin stomps on everything
let b:did_ftplugin = 1

setlocal commentstring=#%s
setlocal iskeyword+=-
setlocal formatoptions+=ro
setlocal foldmethod=syntax
setlocal foldlevelstart=0

" help strip things like "${xxx}"
:map ,xx :normal ds"xds{<CR>

" #,fo open marker; #,fc close; ,fx remove
call s:tools.llnnoremap('fo', ':<C-U>execute ''s/\s*\(\s*#\s*{{{\d*\)*\s*$/ # {{{''.(v:count?v:count:1).''/''')
call s:tools.llnnoremap('fc', ':<C-U>execute ''s/\s*\(\s*#\s*}}}\d*\)*\s*$/ # }}}''.(v:count?v:count:1).''/''')
call s:tools.llnnoremap('fx', ':execute ''s/\s*\(\s*#\s*\(}}}\\|{{{\)\d*\)*\s*$//''')

" ==> surrounds

" o
let b:surround_111  = "one(\r)"

unlet s:tools
" __END__

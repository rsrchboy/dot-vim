" Add signify coloring support
"
" Chris Weyl <cweyl@alumni.drew.edu> 2014

" zenburn: https://github.com/jnurmine/Zenburn
" signify: https://github.com/mhinz/vim-signify

" base changes
hi Conceal      guibg=NONE
hi CursorLineNr guibg=NONE ctermbg=none
hi Directory    guifg=LightBlue
hi FoldColumn   guibg=NONE ctermbg=none
hi Folded       guibg=NONE ctermbg=none
hi Ignore       guibg=NONE guifg=#303030
hi LineNr       guibg=NONE ctermbg=none
hi Normal       guibg=NONE ctermbg=none
hi SignColumn   guibg=NONE ctermbg=none
hi TabLine      guibg=NONE ctermbg=none
hi TabLineFill  guibg=NONE ctermbg=none guifg=Black
hi TabLineSel   guibg=NONE ctermbg=none
hi DiffAdd      cterm=bold ctermbg=none ctermfg=119 term=NONE guibg=NONE guifg=LightGreen
hi DiffDelete   cterm=bold ctermbg=none ctermfg=167 term=NONE guibg=NONE guifg=LightRed
hi DiffChange   cterm=bold ctermbg=none ctermfg=227 term=NONE guibg=NONE guifg=Orange
hi diffAdded    cterm=none ctermbg=none ctermfg=119 term=NONE guibg=NONE guifg=LightGreen
hi diffRemoved  cterm=none ctermbg=none ctermfg=167 term=NONE guibg=NONE guifg=LightRed
hi SpecialKey   guibg=NONE guifg=#303030
" hi DiffChange      cterm=bold ctermbg=none ctermfg=227 gui=NONE guibg=NONE guifg=Orange
" hi NonText          term=bold ctermfg=238 gui=bold guifg=#404040
" hi NonText          guibg=NONE


" vim-signify
hi SignifySignAdd    term=NONE cterm=NONE ctermfg=119 gui=NONE guibg=NONE guifg=LightGreen
hi SignifySignDelete term=NONE cterm=NONE ctermfg=167 gui=NONE guibg=NONE guifg=LightRed
hi SignifySignChange term=NONE cterm=NONE ctermfg=227 gui=NONE guibg=NONE guifg=Orange

" vim-diminactive
hi ColorColumn guibg=#555555


" __END__

if exists('g:cpan_netrw_loaded')
    finish
endif
let g:cpan_netrw_loaded = 1

augroup cpan#netrw
    au!

    " cpan://[dist]/[version]/[module-or-filename]
    "au FileReadCmd,BufReadCmd cpan://* call s:ModuleRead()
    au BufReadCmd cpan://* call s:ModuleRead()
    " au FileReadCmd pod://*    call s:PodRead()
augroup END

fun! s:ModuleRead() abort
    let l:uri = expand('<afile>')
    let l:mod = substitute(l:uri, 'cpan://', '', '')

    set ft=perl
    echom 'cpan: ' . l:uri . ' !! ' . l:mod
    " exe ':Nread https://fastapi.metacpan.org/v1/source/' . l:mod
    exe ':doautocmd BufReadCmd https://fastapi.metacpan.org/v1/source/' . l:mod
endfun

" __END__

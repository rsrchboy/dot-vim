scriptencoding utf-8

if exists('g:loaded_tabtitle')
    finish
endif
let g:loaded_tabtitle = 1

" default "empty" title
let s:noTitle = 'No repository!'

fun! MyTabLabel(n) abort
    let l:buflist = tabpagebuflist(a:n)
    let l:winnr = tabpagewinnr(a:n)
    return fnamemodify(bufname(l:buflist[l:winnr - 1]), ':~')
endfun

fun! MyTabLine() abort
    let l:s = ''
    for l:i in range(tabpagenr('$'))
        " select the highlighting
        let l:page = l:i + 1
        if l:page == tabpagenr()
            let l:s .= '%#TabLineSel#'
        else
            let l:s .= '%#TabLine#'
        endif

        " set the tab page number (for mouse clicks)
        let l:s .= '%' . l:page . 'T'

        " the label is made by MyTabLabel()
        let l:s .= l:page . ': %{gettabvar('.l:page.',"tab_page_title",MyTabLabel(' . l:page . '))} '
        " let l:s .= '%(%{fnamemodify(getcwd(), ":~")} %)'
    endfor

    " after the last tab fill with TabLineFill and reset tab page nr
    let l:s .= '%#TabLineFill#%T'

    " right-align the label to close the current tab page
    if tabpagenr('$') > 1
        let l:s .= '%=%#TabLine#%999Xclose'
    endif

    return l:s
endfun

set tabline=%!MyTabLine()
set guitablabel=%!t:tab_page_title

fun! MyPickTabPageTitleGit()
    if exists('t:git_dir') || get('t:', tab_page_title, s:noTitle) !=# s:noTitle
        return
    endif

    let t:git_dir        = FugitiveGitDir()
    let t:git_commondir  = FugitiveCommonDir()
    let t:git_workdir    = FugitiveWorkTree()
    let t:tab_page_title = '± ' . fnamemodify(t:git_commondir, ':h:t')

    if has_key(t:, 'tab_page_subtitle')
        let t:tab_page_title .= '[' . t:tab_page_subtitle . ']'
    endif

    " echom "t:git_workdir == '" . t:git_workdir . "'"
    " echom "tweaked ==       '" . fnamemodify(t:git_dir, ':h') . "'"
    if t:git_workdir !=# fnamemodify(t:git_dir, ':h')
        let t:tab_page_title .= '('.fnamemodify(t:git_workdir, ':t').')'
    endif

    return
endfun

fun! MyResetTabTitle() abort
    if &buftype !=# '' || get(t:, 'tab_page_title_override', 0)
        return
    endif
    silent! unlet t:tab_page_title t:git_dir t:git_commondir t:git_workdir
    call MyPickTabPageTitleGit()
    return
endfun

fun! tabtitle#ResetAll() abort
    tabdo TabTitleReset
endfun

fun! s:titleOverride(title)
    let t:tab_page_title_override = 1
    let t:tab_page_title          = a:title
endfun

augroup tabtitle
    au!

    au User     Fugitive    silent! call MyPickTabPageTitleGit()
    au FileType checkhealth call s:titleOverride('++VIM Health Report++')
    au FileType GV          call s:titleOverride('GV')
    au FileType startify    call s:titleOverride('Startify!')

    au TabEnter *        if !exists('t:tab_page_title') | let t:tab_page_title = s:noTitle | endif
    au TabNew   *        if !exists('t:tab_page_title') | let t:tab_page_title = s:noTitle | endif
    " au BufAdd * something magic that moves buffer to tab based on git workdir

    " TODO need an equivalent for gui options?
    au OptionSet showtabline if v:option_new > 0 | call tabtitle#ResetAll() | endif
augroup END

command! TabTitleReset silent! call MyResetTabTitle()

" __END__

" Name:             ~/.vimrc
" Summary:          My ~/.vimrc and configuration
" Maintainer:       Chris Weyl <cweyl@alumni.drew.edu>
" Canonical Source: https://gitlab.com/rsrchboy/dot-vim
" License:          CC BY-NC-SA 4.0 (aka Attribution-NonCommercial-ShareAlike)

" Vim 8 defaults
unlet! skip_defaults_vim
silent! source $VIMRUNTIME/defaults.vim

" give any spawned shells a clue
let $IN_VIM = exists('$IN_VIM') ? $IN_VIM + 1 : 1

" keep track of where we live
let $VIMHOME = expand('<sfile>:p:h')

" vim-go, etc, install helper binaries here
let $PATH = $VIMHOME . '/bin:' . $PATH

set encoding=utf-8
scriptencoding utf-8

let s:vimdir = expand('<sfile>:p:h')

let g:rsrchboy#fzf#additional_project_dirs = '~/iris ~/sites'

let g:health#rsrchboy#optional_sources = []
let g:health#rsrchboy#sources          = []

fun! s:MaybeSource(file) abort " {{{1
    let l:file = expand(a:file)
    if filereadable(expand(l:file))
        exe 'source' l:file
        let g:health#rsrchboy#optional_sources += [a:file]
    endif
endfun

fun! s:MaybeSourceDir(dir) abort " {{{1
    for l:f in split(glob(a:dir.'/*.vim'), '\n')
        call s:MaybeSource(l:f)
    endfor
endfun

fun! s:SourcePart(part) abort " {{{1
    " source a file that constitutes a part of our vimrc configuration
    let l:file = s:vimdir . '/' . a:part
    exe 'source' l:file
    let g:health#rsrchboy#sources += [fnamemodify(l:file, ':p')]
endfun " }}}1

call s:SourcePart('rsrchboy/autocmds.vim')
call s:SourcePart('rsrchboy/functions.vim')

" load plugins
call plug#begin()
call s:SourcePart('rsrchboy/vimrc-plugins.vim')
call rsrchboy#sourcecfgdir('plugins')
call plug#end()

" Configuration: settings {{{1

set autoindent                 " Preserve current indent on new lines
set autoread                   " reload when changed -- e.g. 'git co ...'
set background=dark
set backspace=indent,eol,start " Make backspaces delete sensibly
set dictionary=/usr/share/dict/words
set expandtab                  " Convert all tabs typed to spaces
set hidden
set ignorecase
set incsearch
set laststatus=2
set lazyredraw
set list
set listchars-=eol:$
set listchars-=tab:>\ ,
set listchars+=tab:\|\ ,trail:-
set matchpairs+=<:>            " Allow % to bounce between angles too
set modeline
set modelines=2
set mouse=""                   " honestly, it annoys me
set noerrorbells
set nostartofline              " try to preserve column on motion commands
set number
set shiftround                 " Indent/outdent to nearest tabstop
set shiftwidth=4               " Indent/outdent by four columns
set showmatch
set smartcase
set smarttab
set softtabstop=4
set spellfile+=~/.vim/spell/en.utf-8.add
set splitright                 " open new vsplit to the right
set tabstop=8
set textwidth=78               " Wrap at this column
set ttimeoutlen=10
set ttyfast
set ttyscroll=3
set whichwrap+=<,>,h,l
" the better to enable git-gutter
set updatetime=100

" XXX reexamine 'noswapfile'
set noswapfile

" Start our folding at level 1, but after that enforce at a high enough level
" that we shouldn't discover our current position has been folded away after
" switching windows
set foldlevelstart=1
set foldlevel=10
set foldcolumn=3

let g:maplocalleader = ','

if &term ==# 'tmux-256color' " {{{1
    set term=xterm-256color
endif

if has('persistent_undo') " {{{1
    set undofile
    set undodir=~/.cache/vim/undo/
    if !isdirectory($HOME.'/.cache/vim/undo')
        silent! call mkdir($HOME.'/.cache/vim/undo', 'p', 0700)
    endif
endif

if has('termguicolors') " {{{1
    set termguicolors
endif " }}}1

call s:SourcePart('rsrchboy/mappings.vim')
call s:SourcePart('rsrchboy/commands.vim')
" rsrchboy/operators.vim handled via autoload trigger in vimrc-plugins

" source any "trial" configurations we may have
call s:MaybeSourceDir(s:vimdir . '/trials.d')

silent! colorscheme zenburn

" source local configs if present
call s:MaybeSourceDir('~/.vimrc.d')
call s:MaybeSource('~/.vimrc.local')
call s:MaybeSource('~/.config/vimrc.local')

set secure
set exrc

" vim: set foldmethod=marker foldcolumn=3 fdls=0 :
